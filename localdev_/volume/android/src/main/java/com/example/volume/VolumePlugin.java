package com.example.volume;

import android.app.Activity;
import android.media.AudioManager;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static android.content.Context.AUDIO_SERVICE;

/**
 * VolumePlugin
 */
public class VolumePlugin implements MethodCallHandler {

    private final MethodChannel channel;
    private Activity activity;
    AudioManager audioManager;
    private int streamType;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "volume");
        channel.setMethodCallHandler(new VolumePlugin(registrar.activity(), channel));
    }

    private VolumePlugin(Activity activity, MethodChannel channel) {
        this.activity = activity;
        this.channel = channel;
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case "controlVolume":
                int streamType = call.argument("streamType");
                controlVolume(streamType);
                break;

            case "getVolume":
                result.success(getVolume());
                break;

            case "setVolume":
                double volume = call.argument("volume");
                int flag = call.argument("flag");
                setVolume(volume, flag);
                break;

            default:
                result.notImplemented();
        }
    }

    /**
     * Change stream type.
     * AudioManager.STREAM_VOICE_CALL, etc
     *
     * @param streamType AudioManager.STREAM_VOICE_CALL, etc
     */
    private void controlVolume(int streamType) {
        this.streamType = streamType;
        activity.setVolumeControlStream(streamType);
    }

    private void initAudioManager() {
        audioManager = (AudioManager)
                activity.getApplicationContext().getSystemService(AUDIO_SERVICE);
    }

    /**
     * Get current volume.
     *
     * @return 0.0 <= volume <= 1.0
     */
    private double getVolume() {
        initAudioManager();
        double curVolume = audioManager.getStreamVolume(streamType);
        double maxVolume = audioManager.getStreamMaxVolume(streamType);

        return curVolume / maxVolume;
    }

    /**
     * Set current volume.
     *
     * @param volume 0.0 <= volume <= 1.0
     * @param flag   AudioManager.FLAG_SHOW_UI, etc
     */
    private void setVolume(double volume, int flag) {
        initAudioManager();

        volume = normalizeVolume(volume);

        int maxVolume = audioManager.getStreamMaxVolume(streamType);
        int newVolume = (int) (volume * maxVolume);
        audioManager.setStreamVolume(streamType, newVolume, flag);
    }

    // clamped value into min <= val <= max
    private static double clamp(double val, double min, double max) {
        return Math.max(min, Math.min(max, val));
    }

    // normalize volume into 0 <= vol <= 1
    private static double normalizeVolume(double volume) {
        return clamp(volume, 0.0f, 1.0f);
    }
}
