import 'dart:async';

import 'package:flutter/services.dart';

class StreamType {
  /// Controls the Voice Call volume
  static const int STREAM_VOICE_CALL = 0;

  /// Controls the system volume
  static const int STREAM_SYSTEM = 1;

  /// Controls the ringer volume
  static const int STREAM_RING = 2;

  /// Controls the media volume
  static const int STREAM_MUSIC = 3;

  // Controls the alarm volume
  static const int STREAM_ALARM = 4;

  /// Controls the notification volume
  static const int STREAM_NOTIFICATION = 5;
}

class Flag {
  /// Show a toast containing the current volume.
  static const int FLAG_SHOW_UI = 1 << 0;

  /// Whether to include ringer modes as possible options when changing volume.
  /// For example, if true and volume level is 0 and the volume is adjusted,
  /// then the ringer mode may switch the silent or vibrate mode.
  static const int FLAG_ALLOW_RINGER_MODES = 1 << 1;

  /// Whether to play a sound when changing the volume.
  static const int FLAG_PLAY_SOUND = 1 << 2;

  /// Removes any sounds/vibrate that may be in the queue, or are playing
  /// (related to changing volume).
  static const int FLAG_REMOVE_SOUND_AND_VIBRATE = 1 << 3;

  /// Whether to vibrate if going into the vibrate ringer mode.
  static const int FLAG_VIBRATE = 1 << 4;
}

/// You can control VoiceCall, System, Ringer, Media, Alarm, Notification
/// volume and get the max possible volumes for the respective.
class Volume {
  static const MethodChannel _channel = const MethodChannel('volume');

  /// Pass any AudioManager Stream as a parameter to this function
  /// to choose stream type.
  static Future<void> controlVolume(int streamType) async {
    var argument = {"streamType": streamType};
    await _channel.invokeMethod('controlVolume', argument);
  }

  /// Get current volume
  /// volume between 0 and 1: 0 <= volume <= 1
  static Future<double> getVolume() async {
    double volume = await _channel.invokeMethod('getVolume');
    return volume;
  }

  /// Set current volume.
  ///
  /// volume between 0 and 1: 0 <= volume <= 1
  /// flag is defined as in [Flag].
  static Future<void> setVolume(double volume,
      [int flag = Flag.FLAG_REMOVE_SOUND_AND_VIBRATE]) async {
    var argument = {"volume": volume, "flag": flag};
    await _channel.invokeMethod('setVolume', argument);
  }
}
