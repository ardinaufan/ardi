import 'dart:async';

import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/material_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:chewie/src/vertical_progress_bar.dart';
import 'package:chewie/src/vertical_progress_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class MaterialControls extends StatefulWidget {
  const MaterialControls({Key key, Function onDownloadClick, Function onSeek,})
      :
        onDownloadClick = onDownloadClick,
        onSeek = onSeek,
        super(key: key);

  final Function onDownloadClick;
  final Function onSeek;

  @override
  State<StatefulWidget> createState() {
    return _MaterialControlsState();
  }
}

class _MaterialControlsState extends State<MaterialControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  bool _hideVolume = true;
  bool _hideBrightness = true;
  bool _hideSkip = true;
  bool _hideSubtitle = false;
  bool _lockScreen = false;
  bool _isDownloaded = true;
  Timer _hideTimer;
  Timer _showTimer;
  Timer _hideTimerVolume;
  Timer _hideTimerBrightness;
  Timer _timerHideSkip;
  Timer _showAfterExpandCollapseTimer;
  bool _dragging = false;

  VideoPlayerController controller;
  ChewieController chewieController;

  final WidgetsBinding wb = WidgetsBinding.instance;

  bool get _isDownloadAble => (_latestValue?.isPlaying ?? false) && !_isDownloaded;

  DragStartDetails horizontalDragStartDetails;
  DragStartDetails verticalDragStartDetails;

  double _currentVolume;
  double _updateVolume;
  double _currentBrightness;
  double _updateBrightness;
  Duration _currentSkip = Duration();
  Duration _updateSkip = Duration();

  static double verticalIndicatorWidth = 50;
  static double halfVerticalIndicatorWidth = verticalIndicatorWidth / 2;

  @override
  void initState() {
    super.initState();

    VideoPlayerController.getDownloadList().then((info) {
      dynamic id = info.firstWhere((dynamic item) =>
      item['id'] == controller.idDownload, orElse: () => null);

      wb.addPostFrameCallback((_) =>
          setState(() => _isDownloaded = id != null)
      );
    });
  }

  @override
  Widget build(BuildContext context) {

    final orientation = MediaQuery.of(context).orientation;
    final buildSize = MediaQuery.of(context).size;
    final barHeight = orientation == Orientation.portrait ? 30.0 : 48.0;

    double w = buildSize.width;
    double h = buildSize.height;

    var boxVolume = {
      'left': w * 0.6,
      'right': w * 0.9,
      'top': h * 0.1,
      'bottom': h * 0.9
    };

    var boxBrightness = {
      'left': w * 0.1,
      'right': w * 0.4,
      'top': h * 0.1,
      'bottom': h * 0.9
    };

    var boxSkip = {
      'left': w * 0.1,
      'right': w * 0.9,
      'top': h * 0.1,
      'bottom': h * 0.9
    };

    final double volumeLeft = w * 0.75 - halfVerticalIndicatorWidth;
    final double brightnessLeft = w * 0.25 - halfVerticalIndicatorWidth;

    return GestureDetector(
      onTap: () => _cancelAndRestartTimer(),
      onHorizontalDragStart: (DragStartDetails details) async {
        print(details.toString());
        horizontalDragStartDetails = details;

        _currentSkip = _latestValue?.position??null;

        double x = details.globalPosition.dx;
        double y = details.globalPosition.dy;

        // check skip
        if (boxSkip['left'] <= x && x <= boxSkip['right'] &&
            boxSkip['top'] <= y && y <= boxSkip['bottom']) {
          _stopTimerHideSkip();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) async {
        if (_lockScreen) return;

        double x = details.globalPosition.dx;
        double y = details.globalPosition.dy;

        double xStart = horizontalDragStartDetails?.globalPosition?.dx;
        double yStart = horizontalDragStartDetails?.globalPosition?.dy;

        // check skip
        if (boxSkip['left'] <= x && x <= boxSkip['right'] &&
            boxSkip['top'] <= y && y <= boxSkip['bottom']) {

          if (_currentSkip != null) {
            int widthInTime = _latestValue.duration.inSeconds ~/ 4;
            int skipSec = ((x - xStart) / w * widthInTime).toInt();
            Duration delta = Duration(seconds: skipSec);

            Duration skip = _currentSkip + delta;
            await chewieController.seekTo(skip);
            if (widget.onSeek != null) {
              widget.onSeek();
            }

            Duration updateSkip = _latestValue.position;
            setState(() {
              _updateSkip = updateSkip;
              _hideSkip = false;
            });
          }
        }
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        if (!_hideSkip) {
          _startTimerHideSkip();
        }
      },
      onVerticalDragStart: (DragStartDetails details) async {
        verticalDragStartDetails = details;

//        if(controller.connected){
//          _currentVolume = await controller.getVolume();
//        }else {
          _currentVolume = await chewieController.getVolume();
//        }
        _currentBrightness = await chewieController.getBrightness();

        double x = details.globalPosition.dx;
        double y = details.globalPosition.dy;

        // check volume
        if (boxVolume['left'] <= x && x <= boxVolume['right'] &&
            boxVolume['top'] <= y && y <= boxVolume['bottom']) {
          _hideTimerVolume?.cancel();
        }

        // check brightness
        if (boxBrightness['left'] <= x && x <= boxBrightness['right'] &&
            boxBrightness['top'] <= y && y <= boxBrightness['bottom']) {
          _hideTimerBrightness?.cancel();
        }
      },
      onVerticalDragUpdate: (DragUpdateDetails details) async {
        if (_lockScreen) return;

        double x = details.globalPosition.dx;
        double y = details.globalPosition.dy;

        double xStart = verticalDragStartDetails?.globalPosition?.dx;
        double yStart = verticalDragStartDetails?.globalPosition?.dy;

        // check volume
        if (boxVolume['left'] <= x && x <= boxVolume['right'] &&
            boxVolume['top'] <= y && y <= boxVolume['bottom']) {

          if (_currentVolume != null) {
            double volume = _currentVolume + (yStart - y) / h;
            chewieController.setVolume(volume);
            controller.setVolume(volume);
            double updateVolume;
//            if(controller.connected){
//              updateVolume = await controller.getVolume();
//            }else {
              updateVolume = await chewieController.getVolume();
//            }
            setState(() {
              _updateVolume = updateVolume;
              _hideVolume = false;
            });
          }
        }

        // check brightness
        if (boxBrightness['left'] <= x && x <= boxBrightness['right'] &&
            boxBrightness['top'] <= y && y <= boxBrightness['bottom']) {

          if (_currentBrightness != null) {
            double brightness = _currentBrightness + (yStart - y) / h;
            chewieController.setBrightness(brightness);

            double updateBrightness = await chewieController.getBrightness();
            setState(() {
              _updateBrightness = updateBrightness;
              _hideBrightness = false;
            });
          }
        }
      },
      onVerticalDragEnd: ((_) {
        if (!_hideVolume) {
          _startHideTimerVolume();
        }

        if (!_hideBrightness) {
          _startHideTimerBrightness();
        }
      }),
      child: AbsorbPointer(
        absorbing: _hideStuff,
        child: Column(
            children: [
              Expanded(
                  child: Stack(children: [
                    _buildErrorIndicator(),
                    _buildSkipIndicator(),
                    _buildVolumeIndicator(verticalIndicatorWidth, volumeLeft),
                    _buildBrightnessIndicator(verticalIndicatorWidth, brightnessLeft),
                    _buildPlayIndicator(),
                    _buildProgressIndicator(),
                    _buildSubtitle(orientation)
                  ])
              ),
              _buildBottomBar(context, barHeight)
            ]
        )
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    print("dispose mc");
    controller.removeListener(_updateState);
    _hideTimer?.cancel();
    _showTimer?.cancel();
    _hideTimerVolume?.cancel();
    _hideTimerBrightness?.cancel();
    _showAfterExpandCollapseTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildBottomBar(BuildContext context, double barHeight) {
    final iconColor = Theme
        .of(context)
        .textTheme
        .button
        .color;

    return _hideStuff ? Container() :
    AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 500),
      child: Container(
        height: barHeight,
        color: Theme
            .of(context)
            .dialogBackgroundColor,
        child: Row(
          children: <Widget>[
            _buildPlayPause(controller, barHeight),
            chewieController.isLive
                ? Expanded(child: const Text('LIVE'))
                : _buildPosition(iconColor),
            chewieController.isLive
                ? const SizedBox()
                : _buildProgressBar(),
            chewieController.allowLock
                ? _buildLockButton(context, controller, barHeight)
                : Container(),
            chewieController.allowDownload
                ? _buildDownloadButton(context, controller, barHeight)
                : Container(),
            chewieController.allowSubtitle
                ? _buildSubtitleButton(controller, barHeight)
                : Container(),
            chewieController.allowMuting
                ? _buildMuteButton(controller, barHeight)
                : Container(),
            chewieController.allowCast && controller.value.allowcast
                ?  _buildCastButton(barHeight)
                : Container(),
            chewieController.allowFullScreen
                ? _buildExpandButton(barHeight)
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget _buildProgressIndicator() {
    if(_latestValue?.hasError ?? true) return Container();

    if ((_latestValue?.duration ?? null) == null ||
        (_latestValue?.isBuffering ?? true)) {
      return Center(child: CircularProgressIndicator());
    }

    return Container();
  }

  Widget _buildVolumeIndicator(double width, double left) {
    double percentVolume = (_updateVolume ?? 0) * 100;
    return Positioned(
        top: 10,
        bottom: 10,
        left: left,
        child: AnimatedOpacity(
            opacity: _hideVolume ? 0.0 : 1.0,
            duration: Duration(milliseconds: 500),
            child: VerticalProgressBar(
              width: width,
              style: VerticalProgressBarStyle(
                backgroundProgress: Color(0x66F0E68C),
                colorBorder: Color(0x66FFD700),
                colorProgress: Color(0x66FF8C00),
                colorProgressDark: Color(0x66DAA520),
              ),
              childCenter: Text(
                "${percentVolume.toStringAsFixed(0)}%",
                style: TextStyle(color: Color(0xff000000)),
              ),
              percent: percentVolume,
              milliseconds: 0,
            ))
    );
  }

  Widget _buildBrightnessIndicator(double width, double left) {
    double percentVolume = (_updateBrightness ?? 0) * 100;
    return Positioned(
        top: 10,
        bottom: 10,
        left: left,
        child: AnimatedOpacity(
            opacity: _hideBrightness ? 0.0 : 1.0,
            duration: Duration(milliseconds: 500),
            child: VerticalProgressBar(
              width: width,
              style: VerticalProgressBarStyle(
                backgroundProgress: Color(0x66F0E68C),
                colorBorder: Color(0x66FFD700),
                colorProgress: Color(0x66FF8C00),
                colorProgressDark: Color(0x66DAA520),
              ),
              childCenter: Text(
                "${percentVolume.toStringAsFixed(0)}%",
                style: TextStyle(color: Color(0xff000000)),
              ),
              percent: percentVolume,
              milliseconds: 0,
            ))
    );
  }

  Widget _buildErrorIndicator() {
    if (!_latestValue.hasError) return Container();

    if (chewieController.errorBuilder != null) {
      return chewieController.errorBuilder(
        context,
        chewieController.videoPlayerController.value.errorDescription,
      );
    }

    return Center(
      child: Icon(
        Icons.error,
        color: Colors.white,
        size: 42,
      ),
    );
  }

  Widget _buildSkipIndicator([int milliseconds = 500]) {
    int sec = (_updateSkip - _currentSkip).inSeconds;
    bool isPositive = sec >= 0;

    sec = sec.abs();
    int min = sec ~/ 60;
    int hour = min ~/ 60;

    sec %= 60;
    min %= 60;

    String text = sec == 0 ? "" :
    ((isPositive ? "+" : "-") +
        (hour > 0 ? "${hour}h " : "") +
        (min > 0 ? "${min}m " : "") +
        (sec > 0 ? "${sec}s" : ""));

    if(controller.connected){
      _hideSkip = false;
    }
    return Center(
        child: AnimatedOpacity(
          opacity: _hideSkip ? 0.0 : 1.0,
          duration: Duration(milliseconds: milliseconds),
            child: Text(
                text,
                style: TextStyle(
                  fontSize: 30,
                  fontFamily: "Monospace",
                  color: Color(0xaad5d916)
                )
          )
        )
    );
  }

  GestureDetector _buildExpandButton(double barHeight) {
    return GestureDetector(
      onTap: _onExpandCollapse,
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          margin: EdgeInsets.only(right: 12.0),
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: Center(
            child: Icon(
              chewieController.isFullScreen
                  ? Icons.fullscreen_exit
                  : Icons.fullscreen,
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildCastButton(double barHeight) {
//    print(controller.connected.toString());
    return GestureDetector(
      onTap: (){
        controller.connected ? controller.disconnect() : showDialog(context: context,builder: (_)=> builddialog(context));
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          color: Colors.transparent,
          margin: EdgeInsets.only(right: 10.0),
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: Center(
            child:
            Icon(
                controller.connected ? Icons.cast_connected : Icons.cast
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildListViewItem(BuildContext context, int index) {
    var name = controller.name_cast.elementAt(index).values.first;
    print(name);
    print(1);
    return ListTile(
      title: Text(name.toString()),
      onTap: (){
        controller.connect(index);
        Navigator.of(context).pop();
      },
    );
  }

  Widget builddialog(BuildContext context) {
    print(controller.name_cast.length);
    return Dialog(
      child: Container(
        height: 200.0,
        width: 100.0,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Pick a casting device', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
          ),
          Divider(height: 0.0, color: Colors.black,),
          Expanded(
            child: ListView.builder(
              itemBuilder: _buildListViewItem,
              itemCount: controller.name_cast.length,
            ),
          )
        ],),
      ),
    );
  }

  Widget _buildPlayIndicator() {
    return ((_latestValue?.duration ?? null) == null ||
        (_latestValue?.isBuffering ?? true)) ?
    Container() :
    GestureDetector(
      onTap:
      (){ if(controller.connected){

      }else {
        _latestValue != null && _latestValue.isPlaying
            ? _cancelAndRestartTimer
            : () {
          _playPause();

          setState(() {
            _hideStuff = true;
          });
        };
      }
      },
      child: Container(
        color: Colors.transparent,
        child: Center(
          child: controller.connected ? Text("Connect to Chromecast",style: TextStyle(color: Colors.white),):
          AnimatedOpacity(
            opacity:
            !(_latestValue?.isPlaying ?? false) && !_dragging
                ? 1.0
                : 0.0,
            duration: Duration(milliseconds: 300),
            child: GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                  color: Theme
                      .of(context)
                      .dialogBackgroundColor,
                  borderRadius: BorderRadius.circular(48.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Icon(Icons.play_arrow, size: 32.0),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildLockButton(
      BuildContext context,
      VideoPlayerController controller,
      double barHeight) {
    return GestureDetector(
      onTap: () {
        if (_lockScreen) {
          setState(() {
            _lockScreen = false;
          });
        } else {
          setState(() {
            _lockScreen = true;
          });
        }
      },
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 8.0,
          right: 8.0,
        ),
        child: Icon(
          (_lockScreen)
              ? Icons.lock
              : Icons.lock_open,
        ),
      ),
    );
  }

  GestureDetector _buildDownloadButton(
      BuildContext context,
      VideoPlayerController controller,
      double barHeight) {
    return GestureDetector(
      onTap: () {
        if (!_isDownloadAble) return;

        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("Download started, you can checked on navigation menu"),
          backgroundColor: Colors.blueGrey[800],
        ));
        controller.download();
        if (widget.onDownloadClick != null) {
          widget.onDownloadClick();
        }
        wb.addPostFrameCallback((_) => setState(() => _isDownloaded = true));
      },
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 8.0,
          right: 8.0,
        ),
        child: Icon(
          Icons.file_download,
          color: _isDownloadAble
              ? Colors.white
              : Colors.black,
        ),
      ),
    );
  }

  GestureDetector _buildSubtitleButton(VideoPlayerController controller,
      double barHeight) {
    return GestureDetector(
      onTap: () {
        if(controller.connected){
          controller.subtitle();
        }else {
          if (_hideSubtitle) {
            setState(() {
              _hideSubtitle = false;
            });
          } else {
            setState(() {
              _hideSubtitle = true;
            });
          }
        }
        },
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 8.0,
          right: 8.0,
        ),
        child: Icon(
          Icons.closed_caption,
          color: controller.connected ? controller.sub
              ? Colors.black
              : Colors.white
          : _hideSubtitle
              ? Colors.black
              : Colors.white,
        ),
      ),
    );
  }

  GestureDetector _buildMuteButton(VideoPlayerController controller,
      double barHeight) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if(controller.connected){
          if (_latestValue.mute == 0) {
            controller.setMute(false);
          } else {
            _latestVolume = controller.value.volume;
            controller.setMute(true);
          }
        }else {
          if (_latestValue.volume == 0) {
            controller.setVolume(_latestVolume ?? 0.5);
          } else {
            _latestVolume = controller.value.volume;
            controller.setVolume(0.0);
          }
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            child: Container(
              height: barHeight,
              padding: EdgeInsets.only(
                left: 8.0,
                right: 8.0,
              ),
              child: Icon(
                (_latestValue != null && _latestValue.volume > 0)
                    ? Icons.volume_up
                    : Icons.volume_off,
              ),
            ),
          ),
        ),
      ),
    );
  }

  GestureDetector _buildPlayPause(VideoPlayerController controller,
      double barHeight) {
    return GestureDetector(
      onTap: _playPause,
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        margin: EdgeInsets.only(left: 8.0, right: 4.0),
        padding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        child: Icon(
          controller.connected ? controller.value.play ? Icons.pause : Icons.play_arrow
              : controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }

  Widget _buildPosition(Color iconColor) {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;
    final duration = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration
        : Duration.zero;

    return Padding(
      padding: EdgeInsets.only(right: 24.0),
      child: Text(
        '${formatDuration(position)} / ${formatDuration(duration)}',
        style: TextStyle(
          fontSize: 14.0,
        ),
      ),
    );
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: MaterialVideoProgressBar(
          controller,
          onDragStart: () {
            setState(() {
              _dragging = true;
            });

            _hideTimer?.cancel();
          },
          onDragEnd: () {
            setState(() {
              _dragging = false;
            });

            _startHideTimer();
          },
          onSeek: () => widget.onSeek(),
          colors: chewieController.materialProgressColors ??
              ChewieProgressColors(
                  playedColor: Theme
                      .of(context)
                      .accentColor,
                  handleColor: Theme
                      .of(context)
                      .accentColor,
                  bufferedColor: Theme
                      .of(context)
                      .backgroundColor,
                  backgroundColor: Theme
                      .of(context)
                      .disabledColor),
        ),
      ),
    );
  }

  Widget _buildSubtitle(Orientation orientation) {
    return chewieController.allowSubtitle && _hideSubtitle == false ?
    Positioned(
        left: 0,
        right: 0,
        bottom: 0,
        child: Container(
            padding: EdgeInsets.only(bottom: 5),
            child: Text(
              _latestValue.subtitle,
              style: TextStyle(
                  fontSize: (orientation == Orientation.portrait) ? 8 : 16,
                  color: Colors.yellow,
                  shadows: (orientation == Orientation.portrait) ? [] : [
                    Shadow(
                        offset: Offset(-1.5, -1.5),
                        color: Colors.black
                    ),
                    Shadow(
                        offset: Offset(1.5, -1.5),
                        color: Colors.black
                    ),
                    Shadow(
                        offset: Offset(1.5, 1.5),
                        color: Colors.black
                    ),
                    Shadow(
                        offset: Offset(-1.5, 1.5),
                        color: Colors.black
                    ),
                  ]
              ),
              textAlign: TextAlign.center,
            )
        )
    ) :
    Container();
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController.autoPlay) {
      _startHideTimer();
    }

    _showTimer = Timer(Duration(milliseconds: 200), () {
      setState(() {
        _hideStuff = false;
      });
    });
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController.toggleFullScreen();
      _showAfterExpandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  void _playPause() {
    setState(() {
      if(controller.connected){
        if(controller.value.play){
          controller.pause();
        }else {
          controller.play();
        }
      }else {
        if (controller.value.isPlaying) {
          _hideStuff = false;
          _hideTimer?.cancel();
          controller.pause();
        } else {
          _cancelAndRestartTimer();

          if (!controller.value.initialized) {
            controller.initialize().then((_) {
              controller.play();
            });
          } else {
            controller.play();
          }
        }
      }
    });
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _startHideTimerVolume() {
    _hideTimerVolume = Timer(const Duration(seconds: 3), () {
      setState(() => _hideVolume = true);
    });
  }

  void _startHideTimerBrightness() {
    _hideTimerBrightness = Timer(const Duration(seconds: 3), () {
      setState(() => _hideBrightness = true);
    });
  }

  void _startTimerHideSkip([int seconds = 1]) {
    _timerHideSkip = Timer(
        Duration(seconds: seconds), () => setState(() => _hideSkip = true)
    );
  }

  void _stopTimerHideSkip() {
    _timerHideSkip?.cancel();
  }

  void _updateState() {
    setState(() {
      _latestValue = controller.value;
    });
  }

}