import 'package:chewie/src/vertical_progress_bar_style.dart';
import 'package:flutter/material.dart';

class VerticalProgressBar extends StatefulWidget {
  VerticalProgressBar(
      {this.percent = 40,
      this.width = 50,
      this.style,
      this.theme,
      this.margin,
      this.reverse = false,
      this.childCenter,
      this.childBottom,
      this.childTop,
      this.milliseconds = 500,
      this.borderRadius,
      this.paddingChildBottom,
      this.paddingChildTop}) {
    assert(percent >= 0);
    assert(width > 0);
  }

  final double percent;
  final double width;
  final VerticalProgressBarStyle style;
  final VerticalProgressBarTheme theme;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry paddingChildBottom;
  final EdgeInsetsGeometry paddingChildTop;
  final Widget childCenter;
  final Widget childBottom;
  final Widget childTop;
  final bool reverse;
  final int milliseconds;
  final BorderRadiusGeometry borderRadius;

  @override
  State<StatefulWidget> createState() => VerticalProgressBarState();
}

class VerticalProgressBarState extends State<VerticalProgressBar> {
  double height;
  double maxWidth;
  double heightProgress;
  VerticalProgressBarStyle style;
  Widget childCenter;
  AlignmentGeometry alignment = AlignmentDirectional.bottomCenter;
  BorderRadiusGeometry borderRadius;
  EdgeInsetsGeometry paddingChildBottom;
  EdgeInsetsGeometry paddingChildTop;

  @override
  void initState() {
    if (widget.style == null) {
      style = VerticalProgressBarStyle();
    } else {
      style = widget.style;
    }

    if (widget.theme != null) {
      switch (widget.theme) {
        case VerticalProgressBarTheme.blue:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressBlue,
              colorProgressDark: colorProgressBlueDark,
              colorBorder: colorBorderDefault);
          break;
        case VerticalProgressBarTheme.red:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressRed,
              colorProgressDark: colorProgressRedDark,
              colorBorder: colorBorderDefault);
          break;
        case VerticalProgressBarTheme.green:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressGreen,
              colorProgressDark: colorProgressGreenDark,
              colorBorder: colorBorderDefault);
          break;
        case VerticalProgressBarTheme.purple:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressPurple,
              colorProgressDark: colorProgressPurpleDark,
              colorBorder: colorBorderDefault);
          break;
        case VerticalProgressBarTheme.yellow:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressYellow,
              colorProgressDark: colorProgressYellowDark,
              colorBorder: colorBorderDefault);
          break;
        case VerticalProgressBarTheme.midnight:
          style = VerticalProgressBarStyle(
              backgroundProgress: backgroundProgressDefault,
              colorProgress: colorProgressMidnight,
              colorProgressDark: colorProgressMidnightDark,
              colorBorder: colorBorderDefault);
          break;
      }
    }

    if (widget.reverse) {
      alignment = AlignmentDirectional.topCenter;
    }

    borderRadius = widget.borderRadius == null
        ? BorderRadius.circular(12)
        : widget.borderRadius;

    paddingChildBottom = widget.paddingChildBottom == null
        ? EdgeInsets.all(16)
        : widget.paddingChildBottom;

    paddingChildTop = widget.paddingChildTop == null
        ? EdgeInsets.all(16)
        : widget.paddingChildTop;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    heightProgress = height * widget.percent / 100;

    return Container(
        margin: widget.margin,
        decoration:
            BoxDecoration(borderRadius: borderRadius, color: style.colorBorder),
        padding: EdgeInsets.all(style.borderWidth),
        child: Container(
            constraints: BoxConstraints.expand(width: widget.width),
            decoration: BoxDecoration(
                borderRadius: borderRadius, color: style.backgroundProgress),
            child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
              Expanded(
                  child: Stack(alignment: alignment, children: <Widget>[
                AnimatedContainer(
                    duration: Duration(milliseconds: widget.milliseconds),
                    height: heightProgress + style.heightShadow,
                    decoration: BoxDecoration(
                        borderRadius: borderRadius,
                        color: style.colorProgressDark)),
                AnimatedContainer(
                  duration: Duration(milliseconds: widget.milliseconds),
                  height: heightProgress,
                  decoration: BoxDecoration(
                      borderRadius: borderRadius, color: style.colorProgress),
                ),
                Center(child: widget.childCenter),
                Padding(
                  padding: paddingChildBottom,
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: widget.childBottom),
                ),
                Padding(
                  padding: paddingChildTop,
                  child: Align(
                      alignment: Alignment.topCenter, child: widget.childTop),
                )
              ]))
            ])));
  }
}
