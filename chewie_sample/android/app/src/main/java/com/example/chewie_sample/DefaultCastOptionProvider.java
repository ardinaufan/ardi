package com.example.chewie_sample;

import android.content.Context;
import android.util.Log;

import com.example.chewie_sample.MainActivity;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.OptionsProvider;
import com.google.android.gms.cast.framework.SessionProvider;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.NotificationOptions;

import java.util.List;

import io.flutter.plugin.common.PluginRegistry;

/**
 * A convenience {@link OptionsProvider} to target the default cast receiver app.
 */
public final class DefaultCastOptionProvider implements OptionsProvider {

    @Override
    public CastOptions getCastOptions(Context context) {

        MainActivity m = new MainActivity();

        NotificationOptions notificationOptions = new NotificationOptions.Builder()
                .setTargetActivityClassName(m.getClass().getName())
                .build();
        CastMediaOptions mediaOptions = new CastMediaOptions.Builder()
                .setNotificationOptions(notificationOptions)
                .setExpandedControllerActivityClassName(m.getClass().getName())
                .build() ;

        return new CastOptions.Builder()
                .setReceiverApplicationId(CastMediaControlIntent.DEFAULT_MEDIA_RECEIVER_APPLICATION_ID)
                .setCastMediaOptions(mediaOptions)
                .build();
    }

    @Override
    public List<SessionProvider> getAdditionalSessionProviders(Context context) {
        return null;
    }

}

