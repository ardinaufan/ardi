import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'chewie_list_item.dart';
import 'package:chewie/chewie.dart';

void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //test();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.blueGrey
      ),
      home: MyHomePage(title: 'Flutter Demo Youtube'),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  var url, res;
  ChewieController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }
 @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
          ],
        ),
        body: ListView(
            children: <Widget>[
              ChewieListItem(
                videoPlayerController: VideoPlayerController.network('http://dev.motion.co.id/Video/tt4154664-captain-marvel-2019-1559111794.MP4'),
                looping: true,
              ),
            ]
        ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    print("dispose");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    ChewieListItem c = ChewieListItem();
    if(state == AppLifecycleState.paused){
//      controller.widgetpause();
      c.videoPlayerController.widgetPause();
    }else if(state == AppLifecycleState.resumed){
//      controller.widgetresume();
      c.videoPlayerController.widgetResume();
    }else if(state == AppLifecycleState.suspending){
//      controller.widgetstop();
      c.videoPlayerController.widgetStop();
    }
  }
}