// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

final MethodChannel _channel = const MethodChannel('flutter.io/videoPlayer')
// This will clear all open videos on the platform when a full restart is
// performed.
// TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
// https://github.com/flutter/flutter/issues/26431
// ignore: strong_mode_implicit_dynamic_method
  ..invokeMethod('init');

class DurationRange {
  DurationRange(this.start, this.end);

  final Duration start;
  final Duration end;

  double startFraction(Duration duration) {
    return start.inMilliseconds / duration.inMilliseconds;
  }

  double endFraction(Duration duration) {
    return end.inMilliseconds / duration.inMilliseconds;
  }

  @override
  String toString() => '$runtimeType(start: $start, end: $end)';
}

/// The duration, current position, buffering state, error state and settings
/// of a [VideoPlayerController].
class VideoPlayerValue {
  VideoPlayerValue(
      {@required this.duration,
      this.size,
      this.position = const Duration(),
      this.buffered = const <DurationRange>[],
      this.isPlaying = false,
      this.isLooping = false,
      this.isBuffering = false,
      this.volume = 1.0,
      this.subtitle = "",
      this.errorDescription,
      this.id_cast,
      this.name_cast,
      this.allowcast = false,
      this.play = true,
      this.mute = false});

  VideoPlayerValue.uninitialized() : this(duration: null);

  VideoPlayerValue.erroneous(String errorDescription)
      : this(duration: null, errorDescription: errorDescription);

  /// The total duration of the video.
  ///
  /// Is null when [initialized] is false.
  final Duration duration;

  /// The current playback position.
  final Duration position;

  /// The currently buffered ranges.
  final List<DurationRange> buffered;

  /// True if the video is playing. False if it's paused.
  final bool isPlaying;

  /// True if the video is looping.
  final bool isLooping;

  /// True if the video is currently buffering.
  final bool isBuffering;

  /// The current volume of the playback.
  final double volume;

  /// The current subtitle of the playback.
  final String subtitle;

  /// The current id cast of the playback.
  final String id_cast;

  /// The current name cast of the playback.
  final String name_cast;

  final bool allowcast;

  final bool play;

  final bool mute;

  /// A description of the error if present.
  ///
  /// If [hasError] is false this is [null].
  final String errorDescription;

  /// The [size] of the currently loaded video.
  ///
  /// Is null when [initialized] is false.
  final Size size;

  bool get initialized => duration != null;

  bool get hasError => errorDescription != null;

  double get aspectRatio => size != null ? size.width / size.height : 1.0;

  VideoPlayerValue copyWith(
      {Duration duration,
      Size size,
      Duration position,
      List<DurationRange> buffered,
      bool isPlaying,
      bool isLooping,
      bool isBuffering,
      double volume,
      String subtitle,
      String errorDescription,
      String id_cast,
      String name_cast,
      bool allowcast,
      bool play,
      Duration cast_seek,
      bool mute}) {
    return VideoPlayerValue(
        duration: duration ?? this.duration,
        size: size ?? this.size,
        position: position ?? this.position,
        buffered: buffered ?? this.buffered,
        isPlaying: isPlaying ?? this.isPlaying,
        isLooping: isLooping ?? this.isLooping,
        isBuffering: isBuffering ?? this.isBuffering,
        volume: volume ?? this.volume,
        subtitle: subtitle ?? this.subtitle,
        errorDescription: errorDescription ?? this.errorDescription,
        id_cast: id_cast ?? this.id_cast,
        name_cast: name_cast ?? this.name_cast,
        allowcast: allowcast ?? this.allowcast,
        play: play ?? this.play,
        mute: mute ?? this.mute
    );
  }

  @override
  String toString() {
    return '$runtimeType('
        'duration: $duration, '
        'size: $size, '
        'position: $position, '
        'buffered: [${buffered.join(', ')}], '
        'isPlaying: $isPlaying, '
        'isLooping: $isLooping, '
        'isBuffering: $isBuffering'
        'volume: $volume, '
        'subtitle: $subtitle, '
        'errorDescription: $errorDescription)';
  }
}

enum DataSourceType { asset, network, file }

class VideoDownloaderValue {
  VideoDownloaderValue({
    this.downloads,
    this.errorDescription,
  });

  VideoDownloaderValue.erroneous(String errorDescription)
      : this(downloads: null, errorDescription: errorDescription);

  /// Download notification.
  final List<Map<String, String>> downloads;

  /// A description of the error if present.
  ///
  /// If [hasError] is false this is [null].
  final String errorDescription;

  VideoDownloaderValue copyWith({
    List<Map<String, String>> downloads,
    String errorDescription,
  }) {
    return VideoDownloaderValue(
      downloads: downloads ?? this.downloads,
      errorDescription: errorDescription ?? this.errorDescription,
    );
  }

  @override
  String toString() {
    return '$runtimeType('
        'download: $downloads, '
        'errorDescription: $errorDescription)';
  }
}

class VideoDownloaderController extends ValueNotifier<VideoDownloaderValue> {
  VideoDownloaderController() : super(VideoDownloaderValue());

  StreamSubscription<dynamic> _eventSubscription;

  Future<void> initialize() async {
    await _channel.invokeMethod<void>('createDownload');

    void eventListener(dynamic event) {
      final Map<dynamic, dynamic> map = event;
      switch (map['event']) {
        case 'onDownloadNotification':
          final List<dynamic> x = List<dynamic>.from(map['downloads']);
          final List<Map<String, String>> downloads = <Map<String, String>>[];
          for (int i = 0; i < x.length; i++) {
            downloads.add(Map<String, String>.from(x[i]));
          }
          value = value.copyWith(downloads: downloads);
          break;
      }
    }

    void errorListener(Object obj) {
      final PlatformException e = obj;
      value = VideoDownloaderValue.erroneous(e.message);
    }

    _eventSubscription =
        const EventChannel("flutter.io/videoPlayer/downloadEvents")
            .receiveBroadcastStream()
            .listen(eventListener, onError: errorListener);
  }

  @override
  Future<void> dispose() async {
    await _eventSubscription?.cancel();
    await _channel.invokeMethod<void>('disposeDownloader');

    super.dispose();
  }
}

/// Controls a platform video player, and provides updates when the state is
/// changing.
///
/// Instances must be initialized with initialize.
///
/// The video is displayed in a Flutter app by creating a [VideoPlayer] widget.
///
/// To reclaim the resources used by the player call [dispose].
///
/// After [dispose] all further calls are ignored.
class VideoPlayerController extends ValueNotifier<VideoPlayerValue> {
  /// Constructs a [VideoPlayerController] playing a video from an asset.
  ///
  /// The name of the asset is given by the [dataSource] argument and must not be
  /// null. The [package] argument must be non-null when the asset comes from a
  /// package and null otherwise.
  VideoPlayerController.asset(this.dataSource, {this.package})
      : dataSourceType = DataSourceType.asset,
        subtitleSource = null,
        idDownload = null,
        super(VideoPlayerValue(duration: null));

  /// Constructs a [VideoPlayerController] playing a video from obtained from
  /// the network.
  ///
  /// The URI for the video is given by the [dataSource] argument and must not be
  /// null.
  VideoPlayerController.network(this.dataSource,
      [this.subtitleSource, this.idDownload])
      : dataSourceType = DataSourceType.network,
        package = null,
        super(VideoPlayerValue(duration: null));

  /// Constructs a [VideoPlayerController] playing a video from a file.
  ///
  /// This will load the file from the file-URI given by:
  /// `'file://${file.path}'`.
  VideoPlayerController.file(File file)
      : dataSource = 'file://${file.path}',
        subtitleSource = null,
        dataSourceType = DataSourceType.file,
        package = null,
        idDownload = null,
        super(VideoPlayerValue(duration: null));

  int _textureId;
  final String dataSource;
  final String subtitleSource;
  final String idDownload;

  bool connected = false;
  bool sub = false;
  int index = 0;

  /// Describes the type of data source this [VideoPlayerController]
  /// is constructed with.
  final DataSourceType dataSourceType;

  final String package;
  Timer _timer;
  bool _isDisposed = false;
  Completer<void> _creatingCompleter;
  StreamSubscription<dynamic> _eventSubscription;
  _VideoAppLifeCycleObserver _lifeCycleObserver;
  final List<Map<String, String>> name_cast = <Map<String, String>>[];
  Map<String, String> test2;

  @visibleForTesting
  int get textureId => _textureId;

  Future<void> initialize() async {
    _lifeCycleObserver = _VideoAppLifeCycleObserver(this);
    _lifeCycleObserver.initialize();
    _creatingCompleter = Completer<void>();
    if (value.name_cast == "Phone" || value.name_cast == null) {
      value.copyWith(allowcast: false);
    }
    Map<dynamic, dynamic> dataSourceDescription;
    switch (dataSourceType) {
      case DataSourceType.asset:
        dataSourceDescription = <String, dynamic>{
          'asset': dataSource,
          'package': package
        };
        break;
      case DataSourceType.network:
        String filePath = subtitleSource;

        if ((subtitleSource != null) && (idDownload != null)) {
          final Directory dir = await getApplicationDocumentsDirectory();
          Directory("${dir.path}/cache").create();

          filePath = "${dir.path}/cache/$idDownload.srt";
          final File fid = File(filePath);

          final bool isExists = fid.existsSync();
          if (!isExists) {
            dynamic resp = await http.get(subtitleSource);
            await fid.writeAsBytes(resp.bodyBytes);
          }
        }

        dataSourceDescription = <String, dynamic>{
          'uri': dataSource,
          'subtitle': filePath,
          'idDownload': idDownload
        };
        break;
      case DataSourceType.file:
        dataSourceDescription = <String, dynamic>{'uri': dataSource};
    }
    // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
    // https://github.com/flutter/flutter/issues/26431
    // ignore: strong_mode_implicit_dynamic_method
    final Map<dynamic, dynamic> response = await _channel.invokeMethod(
      'create',
      dataSourceDescription,
    );
    _textureId = response['textureId'];
    _creatingCompleter.complete(null);
    final Completer<void> initializingCompleter = Completer<void>();

    DurationRange toDurationRange(dynamic value) {
      final List<dynamic> pair = value;
      return DurationRange(
        Duration(milliseconds: pair[0]),
        Duration(milliseconds: pair[1]),
      );
    }

    void eventListener(dynamic event) {
      final Map<dynamic, dynamic> map = event;
      switch (map['event']) {
        case 'initialized':
          value = value.copyWith(
            duration: Duration(milliseconds: map['duration']),
            size: Size(map['width']?.toDouble() ?? 0.0,
                map['height']?.toDouble() ?? 0.0),
          );
          initializingCompleter.complete(null);
          _applyLooping();
          _applyVolume();
          _applyPlayPause();
          break;
        case 'completed':
          value = value.copyWith(isPlaying: false, position: value.duration);
          _timer?.cancel();
          break;
        case 'bufferingUpdate':
          final List<dynamic> values = map['values'];
          value = value.copyWith(
            buffered: values.map<DurationRange>(toDurationRange).toList(),
          );
          break;
        case 'bufferingStart':
          value = value.copyWith(isBuffering: true);
          break;
        case 'bufferingEnd':
          value = value.copyWith(isBuffering: false);
          break;
        case 'subtitle':
          value = value.copyWith(subtitle: map['values']);
          break;
        case 'cast':
          if (map["values_name"] == value.name_cast) {
          }else {
            value = value.copyWith(id_cast: map['values_id']);
            value = value.copyWith(name_cast: map['values_name']);
            test2 = {value.id_cast: value.name_cast};
            if(name_cast.isEmpty && map['values_name'] != "Phone" ){
              name_cast.add(test2);
            }else {
              if(map['values_name'] != "Phone" && name_cast.elementAt(index).values.first != value.name_cast ) {
                name_cast.add(test2);
              }else{
                if(index == name_cast.length){
                  index = 0;
                }else if(index+1 == name_cast.length){
                  index = 0;
                }else{
                  index = index+1;
                }
              }
            }
          }
          if (value.id_cast != null && value.name_cast != null) {
            if (value.name_cast == "Phone") {
              value = value.copyWith(allowcast: false);
            } else {
              value = value.copyWith(allowcast: true);
            }
          }else{
            value = value.copyWith(allowcast: false);
          }
          break;
        case 'cast_remove':
          value = value.copyWith(id_cast: map['values_id']);
          value = value.copyWith(name_cast: map['values_name']);
          value = value.copyWith(allowcast: false);
          name_cast.clear();
          break;
        case 'play':
          value = value.copyWith(play: map['status']);
          break;
        case 'getsession':
          value = value.copyWith(allowcast: map['values']);
          value = value.copyWith(play: map['values']);
          Duration i = Duration(milliseconds: map['values_seek']);
          value = value.copyWith(duration: i);

          getsess(i);
          break;
      }
    }

    void errorListener(Object obj) {
      final PlatformException e = obj;
      value = VideoPlayerValue.erroneous(e.message);
      _timer?.cancel();
    }

    _eventSubscription = _eventChannelFor(_textureId)
        .receiveBroadcastStream()
        .listen(eventListener, onError: errorListener);
    return initializingCompleter.future;
  }

  EventChannel _eventChannelFor(int textureId) {
    return EventChannel('flutter.io/videoPlayer/videoEvents$textureId');
  }

  @override
  Future<void> dispose() async {
    if (_creatingCompleter != null) {
      await _creatingCompleter.future;
      if (!_isDisposed) {
        _isDisposed = true;
        _timer?.cancel();
        await _eventSubscription?.cancel();
        // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
        // https://github.com/flutter/flutter/issues/26431
        // ignore: strong_mode_implicit_dynamic_method
        await _channel.invokeMethod(
          'dispose',
          <String, dynamic>{'textureId': _textureId},
        );
      }
      _lifeCycleObserver.dispose();
    }
    _isDisposed = true;
    value.copyWith(allowcast: false);
    super.dispose();
  }

  Future<void> getsess(Duration dur) async {
    await seekTo(dur);
    connected = value.play;
    await _applyPlayPause();
  }

  Future<void> play() async {
    if (connected) {
      value = value.copyWith(play: true);
    } else {
      value = value.copyWith(isPlaying: true);
    }
    await _applyPlayPause();
  }

  Future<void> setLooping(bool looping) async {
    value = value.copyWith(isLooping: looping);
    await _applyLooping();
  }

  Future<void> pause() async {
    if (connected) {
      value = value.copyWith(play: false);
    } else {
      value = value.copyWith(isPlaying: false);
    }
    await _applyPlayPause();
  }

  Future<void> connect(int index) async {
    connected = true;
    value = value.copyWith(isPlaying: false);
    value = value.copyWith(allowcast: true);
    await _applycast(index);
  }

  Future<void> subtitle() async {
    sub = true;
    await _applySubtitle();
  }

  Future<void> disconnect() async {
    value = value.copyWith(isPlaying: true);
    value = value.copyWith(allowcast: true);
    await _discast();
  }

  Future<void> widgetPause()async{
    value = value.copyWith(isPlaying: value.isPlaying);
    value = value.copyWith(allowcast: value.allowcast);
    value = value.copyWith(play: value.play);
    connected = connected;
    await wpause();
  }

  Future<void> widgetStop()async{
    value = value.copyWith(isPlaying: value.isPlaying);
    value = value.copyWith(allowcast: value.allowcast);
    value = value.copyWith(play: value.play);
    connected = connected;
    await wstop();
  }

  Future<void> widgetResume()async{
    value = value.copyWith(isPlaying: value.isPlaying);
    value = value.copyWith(allowcast: value.allowcast);
    value = value.copyWith(play: value.play);
    connected = connected;
    await wresume();
  }

  Future<void> _applyLooping() async {
    if (!value.initialized || _isDisposed) {
      return;
    }
    // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
    // https://github.com/flutter/flutter/issues/26431
    // ignore: strong_mode_implicit_dynamic_method
    _channel.invokeMethod(
      'setLooping',
      <String, dynamic>{'textureId': _textureId, 'looping': value.isLooping},
    );
  }

  Future<void> _applyPlayPause() async {
    if (connected) {
      if (!value.play) {
        await _channel.invokeMethod(
          'cast_play',
          <String, dynamic>{'textureId': _textureId, 'play': false},
        );
      } else {
        await _channel.invokeMethod(
          'cast_play',
          <String, dynamic>{'textureId': _textureId, 'play': true},
        );
      }
    } else {
      if (!value.initialized || _isDisposed) {
        return;
      }
      if (value.isPlaying) {
        // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
        // https://github.com/flutter/flutter/issues/26431
        // ignore: strong_mode_implicit_dynamic_method
        await _channel.invokeMethod(
          'play',
          <String, dynamic>{'textureId': _textureId},
        );
        _timer = Timer.periodic(
          const Duration(milliseconds: 500),
          (Timer timer) async {
            if (_isDisposed) {
              return;
            }
            final Duration newPosition = await position;
            if (_isDisposed) {
              return;
            }
            value = value.copyWith(position: newPosition);
          },
        );
      } else {
        _timer?.cancel();
        // TODO(amirh): remove this on when the invoke
        //  Method update makes it to stable Flutter.
        // https://github.com/flutter/flutter/issues/26431
        // ignore: strong_mode_implicit_dynamic_method
        await _channel.invokeMethod(
          'pause',
          <String, dynamic>{'textureId': _textureId},
        );
      }
    }
  }

  Future<void> _applycast(int index) async {
    if (!value.initialized || _isDisposed) {
      return;
    }
    await _channel.invokeMethod(
      'cast_connect',
      <String, dynamic>{
        'textureId': _textureId,
        'location': value.position.inMilliseconds,
        'index': index,
      },
    );
  }

  Future<void> wpause() async {
    await _channel.invokeMethod(
      'widgetpause',
      <String,dynamic>{
        'textureId': _textureId,
      },
    );
  }

  Future<void> wresume() async {
    await _channel.invokeMethod(
      'widgetresume',
      <String,dynamic>{
        'textureId': _textureId,
      },
    );
  }

  Future<void> wstop() async {
    await _channel.invokeMethod(
      'widgetstop',
      <String,dynamic>{
        'textureId': _textureId,
      },
    );
  }

  Future<void> _discast() async {
    connected = false;
    if (!value.initialized || _isDisposed) {
      return;
    }
    await _channel.invokeMethod(
      'cast_disconnect',
      <String, dynamic>{'textureId': _textureId, 'name_cast': value.name_cast},
    );
  }

  Future<void> _applyVolume() async {
    if (!value.initialized || _isDisposed) {
      return;
    }
    // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
    // https://github.com/flutter/flutter/issues/26431
    // ignore: strong_mode_implicit_dynamic_method
    if(connected){
      await _channel.invokeMethod(
        'setCastVolume',
        <String, dynamic>{'textureId': _textureId, 'volume': value.volume,'ismute': false},
      );
    }else {
      await _channel.invokeMethod(
        'setVolume',
        <String, dynamic>{'textureId': _textureId, 'volume': value.volume},
      );
    }
  }
  Future<void> _applyMute() async {
    if (!value.initialized || _isDisposed) {
      return;
    }
      await _channel.invokeMethod(
        'setCastMute',
        <String, dynamic>{'textureId': _textureId,'ismute': value.mute},
      );

  }

  Future<void> _applySubtitle() async {
    if (!value.initialized || _isDisposed) {
      return;
    }
    await _channel.invokeMethod(
      'setSub',
      <String, dynamic>{'textureId': _textureId},
    );

  }

  Future<double> getVolume() async {
    double volume = await _channel.invokeMethod(
        'getCastVolume',
        <String, dynamic>{'textureId': _textureId},);

    return volume;
  }

  /// The position in the current video.
  Future<Duration> get position async {
    if (_isDisposed) {
      return null;
    }
    return Duration(
      // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
      // https://github.com/flutter/flutter/issues/26431
      // ignore: strong_mode_implicit_dynamic_method
      milliseconds: await _channel.invokeMethod(
        'position',
        <String, dynamic>{'textureId': _textureId, 'isconnect': connected},
      ),
    );
  }

  Future<void> seekTo(Duration moment) async {
    if (_isDisposed) {
      return;
    }
    if (connected) {
      if (moment > value.duration) {
        moment = value.duration;
      } else if (moment < const Duration()) {
        moment = const Duration();
      }
      // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
      // https://github.com/flutter/flutter/issues/26431
      // ignore: strong_mode_implicit_dynamic_method
      await _channel.invokeMethod('cast_seek_to', <String, dynamic>{
        'textureId': _textureId,
        'location': moment.inMilliseconds,
      });
    } else {
      if (moment > value.duration) {
        moment = value.duration;
      } else if (moment < const Duration()) {
        moment = const Duration();
      }
      // TODO(amirh): remove this on when the invokeMethod update makes it to stable Flutter.
      // https://github.com/flutter/flutter/issues/26431
      // ignore: strong_mode_implicit_dynamic_method
      await _channel.invokeMethod('seekTo', <String, dynamic>{
        'textureId': _textureId,
        'location': moment.inMilliseconds,
      });
      value = value.copyWith(position: moment);
    }
  }

  /// Sets the audio volume of [this].
  ///
  /// [volume] indicates a value between 0.0 (silent) and 1.0 (full volume) on a
  /// linear scale.
  Future<void> setVolume(double volume) async {
    value = value.copyWith(volume: volume.clamp(0.0, 1.0));
    await _applyVolume();
  }

  Future<void> setMute(bool mute) async {
    value = value.copyWith(mute: mute);
    await _applyMute();
  }

  /// Download the selected track.
  ///
  /// [tracks] selected tracks. e.g. tracks: [[0, 0, 0, 7], [0, 1, 0, 0]]
  /// Format: [[period, renderer, group, track], ...]
  /// If no tracks given, it will select the best video and audio track.
  Future<void> download({dynamic tracks, dynamic id}) async {
    await _channel.invokeMethod<void>(
      'download',
      <String, dynamic>{
        'url': dataSource,
        'tracks': tracks,
        'id': id ?? idDownload
      },
    );
  }

  /// Get all the track information.
  ///
  /// e.g.: {periods: [{renderers: [
  /// {groups: [{tracks: [1280 × 720, 2.96 Mbps, 992 × 560, 2.06 Mbps]}], type: 2},
  /// {groups: [{tracks: [Stereo, 0.13 Mbps]}], type: 1}]}]}
  ///
  /// [uri] custom uri to check the track information.
  /// If no uri given, it will use dataSource.
  Future<Map<dynamic, dynamic>> getStreamInfo({dynamic uri}) async {
    return await _channel.invokeMethod<Map<dynamic, dynamic>>(
        'getStreamInfo', <String, String>{'url': uri ?? dataSource});
  }

  /// Get list of downloaded track.
  static Future<List<dynamic>> getDownloadList() async {
    return await _channel.invokeMethod<List<dynamic>>('getDownloadList');
  }

  /// Remove all downloaded track.`
  static Future<void> removeAllDownload() async {
    final Directory dir = await getApplicationDocumentsDirectory();
    String path = "${dir.path}/cache";
    bool isExists = Directory(path).existsSync();
    if (isExists) {
      Directory(path).deleteSync(recursive: true);
    }

    return await _channel.invokeMethod('removeAllDownload');
  }

  /// Remove downloaded certain track.
  static Future<void> removeDownload(String id) async {
    final Directory dir = await getApplicationDocumentsDirectory();
    String path = "${dir.path}/cache/$id.srt";
    bool isExists = File(path).existsSync();
    if (isExists) {
      File(path).deleteSync();
    }

    return await _channel
        .invokeMethod<void>('removeDownload', <String, String>{'id': id});
  }

  /// Pause all download activity.
  static Future<void> pauseAllDownload() async {
    return await _channel.invokeMethod<void>('pauseAllDownload');
  }

  /// Resume all download activity.
  static Future<void> resumeAllDownload() async {
    return await _channel.invokeMethod<void>('resumeAllDownload');
  }

  /// Pause download at certain id
  static Future<void> pauseDownload(String id) async {
    return await _channel
        .invokeMethod<void>('pauseDownload', <String, String>{'id': id});
  }

  /// Resume download at certain id
  static Future<void> resumeDownload(String id) async {
    return await _channel
        .invokeMethod<void>('resumeDownload', <String, String>{'id': id});
  }

  /// Set maximum parallel download.
  static Future<void> setMaxParallelDownloads(int maxParallelDownloads) async {
    return await _channel.invokeMethod<void>('setMaxParallelDownloads',
        <String, int>{'maxParallelDownloads': maxParallelDownloads});
  }
}

class _VideoAppLifeCycleObserver extends Object with WidgetsBindingObserver {
  _VideoAppLifeCycleObserver(this._controller);

  bool _wasPlayingBeforePause = false;
  final VideoPlayerController _controller;

  void initialize() {
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        _wasPlayingBeforePause = _controller.value.isPlaying;
        _controller.pause();
        break;
      case AppLifecycleState.resumed:
        if (_wasPlayingBeforePause) {
          _controller.play();
        }
        break;
      default:
    }
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
  }
}

/// Displays the video controlled by [controller].
class VideoPlayer extends StatefulWidget {
  VideoPlayer(this.controller);

  final VideoPlayerController controller;

  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  _VideoPlayerState() {
    _listener = () {
      final int newTextureId = widget.controller.textureId;
      if (newTextureId != _textureId) {
        setState(() {
          _textureId = newTextureId;
        });
      }
    };
  }

  VoidCallback _listener;
  int _textureId;

  @override
  void initState() {
    super.initState();
    _textureId = widget.controller.textureId;
    // Need to listen for initialization events since the actual texture ID
    // becomes available after asynchronous initialization finishes.
    widget.controller.addListener(_listener);
  }

  @override
  void didUpdateWidget(VideoPlayer oldWidget) {
    super.didUpdateWidget(oldWidget);
    oldWidget.controller.removeListener(_listener);
    _textureId = widget.controller.textureId;
    widget.controller.addListener(_listener);
  }

  @override
  void deactivate() {
    super.deactivate();
    widget.controller.removeListener(_listener);
  }

  @override
  Widget build(BuildContext context) {
    return _textureId == null ? Container() : Texture(textureId: _textureId);
  }
}

class VideoProgressColors {
  VideoProgressColors({
    this.playedColor = const Color.fromRGBO(255, 0, 0, 0.7),
    this.bufferedColor = const Color.fromRGBO(50, 50, 200, 0.2),
    this.backgroundColor = const Color.fromRGBO(200, 200, 200, 0.5),
  });

  final Color playedColor;
  final Color bufferedColor;
  final Color backgroundColor;
}

class _VideoScrubber extends StatefulWidget {
  _VideoScrubber({
    @required this.child,
    @required this.controller,
  });

  final Widget child;
  final VideoPlayerController controller;

  @override
  _VideoScrubberState createState() => _VideoScrubberState();
}

class _VideoScrubberState extends State<_VideoScrubber> {
  bool _controllerWasPlaying = false;

  VideoPlayerController get controller => widget.controller;

  @override
  Widget build(BuildContext context) {
    void seekToRelativePosition(Offset globalPosition) {
      final RenderBox box = context.findRenderObject();
      final Offset tapPos = box.globalToLocal(globalPosition);
      final double relative = tapPos.dx / box.size.width;
      final Duration position = controller.value.duration * relative;
      controller.seekTo(position);
    }

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: widget.child,
      onHorizontalDragStart: (DragStartDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        _controllerWasPlaying = controller.value.isPlaying;
        if (_controllerWasPlaying) {
          controller.pause();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        if (_controllerWasPlaying) {
          controller.play();
        }
      },
      onTapDown: (TapDownDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        seekToRelativePosition(details.globalPosition);
      },
    );
  }
}

/// Displays the play/buffering status of the video controlled by [controller].
///
/// If [allowScrubbing] is true, this widget will detect taps and drags and
/// seek the video accordingly.
///
/// [padding] allows to specify some extra padding around the progress indicator
/// that will also detect the gestures.
class VideoProgressIndicator extends StatefulWidget {
  VideoProgressIndicator(
    this.controller, {
    VideoProgressColors colors,
    this.allowScrubbing,
    this.padding = const EdgeInsets.only(top: 5.0),
  }) : colors = colors ?? VideoProgressColors();

  final VideoPlayerController controller;
  final VideoProgressColors colors;
  final bool allowScrubbing;
  final EdgeInsets padding;

  @override
  _VideoProgressIndicatorState createState() => _VideoProgressIndicatorState();
}

class _VideoProgressIndicatorState extends State<VideoProgressIndicator> {
  _VideoProgressIndicatorState() {
    listener = () {
      if (!mounted) {
        return;
      }
      setState(() {});
    };
  }

  VoidCallback listener;

  VideoPlayerController get controller => widget.controller;

  VideoProgressColors get colors => widget.colors;

  @override
  void initState() {
    super.initState();
    controller.addListener(listener);
  }

  @override
  void deactivate() {
    controller.removeListener(listener);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    Widget progressIndicator;
    if (controller.value.initialized) {
      final int duration = controller.value.duration.inMilliseconds;
      final int position = controller.value.position.inMilliseconds;

      int maxBuffering = 0;
      for (DurationRange range in controller.value.buffered) {
        final int end = range.end.inMilliseconds;
        if (end > maxBuffering) {
          maxBuffering = end;
        }
      }

      progressIndicator = Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          LinearProgressIndicator(
            value: maxBuffering / duration,
            valueColor: AlwaysStoppedAnimation<Color>(colors.bufferedColor),
            backgroundColor: colors.backgroundColor,
          ),
          LinearProgressIndicator(
            value: position / duration,
            valueColor: AlwaysStoppedAnimation<Color>(colors.playedColor),
            backgroundColor: Colors.transparent,
          ),
        ],
      );
    } else {
      progressIndicator = LinearProgressIndicator(
        value: null,
        valueColor: AlwaysStoppedAnimation<Color>(colors.playedColor),
        backgroundColor: colors.backgroundColor,
      );
    }
    final Widget paddedProgressIndicator = Padding(
      padding: widget.padding,
      child: progressIndicator,
    );
    if (widget.allowScrubbing) {
      return _VideoScrubber(
        child: paddedProgressIndicator,
        controller: controller,
      );
    } else {
      return paddedProgressIndicator;
    }
  }
}
