package io.flutter.plugins.videoplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadService;
import com.google.android.exoplayer2.scheduler.PlatformScheduler;
import com.google.android.exoplayer2.scheduler.Scheduler;
import com.google.android.exoplayer2.ui.DownloadNotificationHelper;
import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

import static com.google.android.exoplayer2.offline.Download.STATE_COMPLETED;
import static com.google.android.exoplayer2.offline.Download.STATE_FAILED;

public class VideoPlayerDownloadService extends DownloadService {
    public interface Listener {
        void onDownloadNotification(List<Download> downloads);
    }

    private static final String CHANNEL_ID = "download_channel";
    private static final int JOB_ID = 1;
    private static final int FOREGROUND_NOTIFICATION_ID = 1;

    private static int nextNotificationId = FOREGROUND_NOTIFICATION_ID + 1;

    private DownloadNotificationHelper notificationHelper;

    private static VideoPlayerPlugin videoPlayerPlugin;

    private static CopyOnWriteArraySet<Listener> listeners = new CopyOnWriteArraySet<>();

    public VideoPlayerDownloadService() {
        super(
                FOREGROUND_NOTIFICATION_ID,
                DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL,
                CHANNEL_ID,
                R.string.exo_download_notification_channel_name);

        nextNotificationId = FOREGROUND_NOTIFICATION_ID + 1;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationHelper = new DownloadNotificationHelper(this, CHANNEL_ID);
    }

    @Override
    protected DownloadManager getDownloadManager() {
        return videoPlayerPlugin.getDownloadManager();
    }

    @Nullable
    @Override
    protected Scheduler getScheduler() {
        return Util.SDK_INT >= 21 ? new PlatformScheduler(this, JOB_ID) : null;
    }

    @Override
    protected Notification getForegroundNotification(List<Download> downloads) {
        for (Listener listener : listeners) {
            listener.onDownloadNotification(downloads);
        }

        Intent intent = new Intent(this, getMainActivityClass(this));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return notificationHelper.buildProgressNotification(
                android.R.drawable.ic_menu_save,
                pendingIntent,
                downloads.size() + " video",
                downloads);
    }

    @Override
    protected void onDownloadChanged(Download download) {
        Notification notification;

        for (Listener listener : listeners) {
            List<Download> downloads = new ArrayList<>();
            downloads.add(download);

            listener.onDownloadNotification(downloads);
        }

        switch (download.state) {
            case STATE_COMPLETED:
                notification = notificationHelper.buildDownloadCompletedNotification(
                        android.R.drawable.ic_delete,
                        null,
                        Util.fromUtf8Bytes(download.request.data));
                break;

            case STATE_FAILED:
                notification = notificationHelper.buildDownloadFailedNotification(
                        android.R.drawable.ic_btn_speak_now,
                        null,
                        Util.fromUtf8Bytes(download.request.data));
                break;

            case Download.STATE_DOWNLOADING:
            case Download.STATE_QUEUED:
            case Download.STATE_REMOVING:
            case Download.STATE_RESTARTING:
            case Download.STATE_STOPPED:
            default:
                return;
        }

        NotificationUtil.setNotification(this, nextNotificationId++, notification);
    }

    public static void setVideoPlayerPlugin(VideoPlayerPlugin videoPlayerPlugin) {
        VideoPlayerDownloadService.videoPlayerPlugin = videoPlayerPlugin;
    }

    private static Class getMainActivityClass(Context context) {
        String packageName = context.getPackageName();
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        String className = launchIntent.getComponent().getClassName();
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void addListener(Listener listener) {
        listeners.add(listener);
    }

    public static void removeListener(Listener listener) {
        listeners.remove(listener);
    }
}
