// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package io.flutter.plugins.videoplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.Surface;

import androidx.appcompat.app.AppCompatActivity;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;

import extcast.CastPlayer;
import extcast.MediaItem;

import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.offline.DefaultDownloadIndex;
import com.google.android.exoplayer2.offline.DefaultDownloaderFactory;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadCursor;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.offline.DownloadService;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider;
import com.google.android.exoplayer2.ui.TrackNameProvider;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.widget.ExpandedControllerActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.TextureRegistry;

import static com.google.android.exoplayer2.DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
import static com.google.android.exoplayer2.Player.REPEAT_MODE_ALL;
import static com.google.android.exoplayer2.Player.REPEAT_MODE_OFF;

public class VideoPlayerPlugin implements MethodCallHandler, PlayerManager.Listener {

    private static final String TAG = "VideoPlayerPlugin";
    private PlayerManager playerManager;

    private QueuingEventSink eventSink = new QueuingEventSink();

    private MediaRouter.RouteInfo route;

    private final MediaItem.Builder mediaItemBuilder;

    private static final String DOWNLOAD_CONTENT_DIRECTORY = "download";

    private Cache downloadCache;

    private DownloadManager downloadManager;

    protected String userAgent;

    protected boolean connected;

    private CastSession mCastSession;


    private DownloadTracker downloadTracker;

    private DatabaseProvider databaseProvider;

    private File downloadDirectory;

    //  FOR CAST
    private MediaRouter mRouter;
    private MediaRouter.Callback mCallback;
    private MediaRouteSelector mSelector;
    private List<MediaRouter.RouteInfo> routeInfos;
    private CastContext castContext;
    public String link;

    @Override
    public void onQueuePositionChanged(int previousIndex, int newIndex) {

    }

    @Override
    public void onQueueContentsExternallyChanged() {

    }

    @Override
    public void onPlayerError() {
    }

    private static class VideoDownloader implements VideoPlayerDownloadService.Listener {

        private QueuingEventSink downloadEventSink = new QueuingEventSink();

        private final EventChannel eventChannel;

        VideoDownloader(EventChannel eventChannel) {
            this.eventChannel = eventChannel;

            setupVideoDownloader(eventChannel);
        }

        private void setupVideoDownloader(EventChannel eventChannel) {
            eventChannel.setStreamHandler(
                    new EventChannel.StreamHandler() {
                        @Override
                        public void onListen(Object o, EventChannel.EventSink sink) {
                            downloadEventSink.setDelegate(sink);
                        }

                        @Override
                        public void onCancel(Object o) {
                            downloadEventSink.setDelegate(null);
                        }
                    });

            VideoPlayerDownloadService.addListener(this);
        }

        @Override
        public void onDownloadNotification(List<Download> downloads) {
            if (downloads != null) {
                HashMap<String, Object> event = new HashMap<>();
                event.put("event", "onDownloadNotification");

                List<Object> downloads_ = new ArrayList<>();
                for (Download download : downloads) {
                    HashMap<String, String> item = new HashMap<>();
                    item.put("uri", download.request.uri.toString());
                    item.put("id", download.request.id);
                    item.put("state", String.valueOf(download.state));
                    item.put("progress", String.valueOf(download.getPercentDownloaded()));
                    item.put("byte", String.valueOf(download.getBytesDownloaded()));

                    downloads_.add(item);
                }
                event.put("downloads", downloads_);

                downloadEventSink.success(event);
            }
        }

        void dispose() {
            VideoPlayerDownloadService.removeListener(this);

            if (eventChannel != null) {
                eventChannel.setStreamHandler(null);
            }
        }
    }

    public class VideoPlayer extends ExpandedControllerActivity {

        private SimpleExoPlayer exoPlayer;

        private Surface surface;

        private final TextureRegistry.SurfaceTextureEntry textureEntry;

//    private QueuingEventSink eventSink = new QueuingEventSink();

        private final EventChannel eventChannel;

        private boolean isInitialized = false;

        private VideoPlayerPlugin plugin;

        VideoPlayer(
                Context context,
                EventChannel eventChannel,
                TextureRegistry.SurfaceTextureEntry textureEntry,
                String dataSource,
                String subtitleSource,
                String idDownload,
                VideoPlayerPlugin plugin,
                Result result) {
            this.eventChannel = eventChannel;
            this.textureEntry = textureEntry;
            this.plugin = plugin;

            TrackSelector trackSelector = new DefaultTrackSelector();
            exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
            castContext = CastContext.getSharedInstance(context);

            Uri uri = Uri.parse(dataSource);
            link = dataSource;

            DataSource.Factory dataSourceFactory;
            if (isFileOrAsset(uri)) {
                dataSourceFactory = new DefaultDataSourceFactory(context, plugin.userAgent);
            } else {
                    dataSourceFactory = plugin.buildCacheHttpDataSourceFactory();
            }

            MediaSource mediaSource = buildMediaSource(uri, dataSourceFactory, context, idDownload);
            if (subtitleSource != null) {
                Format format = Format.createTextSampleFormat(
                        null,
                        MimeTypes.APPLICATION_SUBRIP,
                        Format.NO_VALUE,
                        null);

                SingleSampleMediaSource.Factory factory =
                        new SingleSampleMediaSource.Factory(dataSourceFactory);

                MediaSource subtitleMediaSource =
                        factory.createMediaSource(Uri.parse(subtitleSource), format, C.TIME_UNSET);

                mediaSource = new MergingMediaSource(mediaSource, subtitleMediaSource);
            }
            exoPlayer.prepare(mediaSource);

            exoPlayer.addTextOutput((TextRenderer.Output) cues -> {
                if (cues != null && cues.size() > 0) {
                    Map<String, Object> event = new HashMap<>();
                    event.put("event", "subtitle");
                    event.put("values", cues.get(0).text.toString());
                    eventSink.success(event);
                }
            });

            setupVideoPlayer(eventChannel, textureEntry, result);
            mRouter = MediaRouter.getInstance(context);
            mCallback = new MyCallback();
            mSelector = castContext.getMergedSelector();

            mRouter.addCallback(mSelector, mCallback, MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
            route = mRouter.updateSelectedRoute(mSelector);
        }

        private final class MyCallback extends MediaRouter.Callback {

            @Override
            public void onProviderChanged(MediaRouter router, MediaRouter.ProviderInfo provider) {
                super.onProviderChanged(router, provider);
                Map<String, Object> event = new HashMap<>();
                routeInfos = provider.getRoutes();
                for (MediaRouter.RouteInfo y : routeInfos) {
                    event.put("event", "cast");
                    event.put("values_id", y.getId());
                    event.put("values_name", y.getName());
                    eventSink.success(event);
                }
            }

            @Override
            public void onRouteChanged(MediaRouter router, MediaRouter.RouteInfo route) {
                super.onRouteChanged(router, route);
            }

            @Override
            public void onRouteRemoved(MediaRouter router, MediaRouter.RouteInfo route) {
                super.onRouteRemoved(router, route);
                Map<String, Object> event = new HashMap<>();
                event.put("event", "cast_remove");
                event.put("values_id", null);
                event.put("values_name", null);
                eventSink.success(event);
            }

            @Override
            public void onProviderAdded(MediaRouter router, MediaRouter.ProviderInfo provider) {
                super.onProviderAdded(router, provider);
            }
        }


        private boolean isFileOrAsset(Uri uri) {
            if (uri == null || uri.getScheme() == null) {
                return false;
            }
            String scheme = uri.getScheme();
            return scheme.equals("file") || scheme.equals("asset");
        }

        private MediaSource buildMediaSource(
                Uri uri, DataSource.Factory mediaDataSourceFactory, Context context, String idDownload) {
            DownloadRequest downloadRequest = plugin.getDownloadTracker().getDownloadRequest(idDownload);
            if (downloadRequest != null) {
                return DownloadHelper.createMediaSource(downloadRequest, mediaDataSourceFactory);
            }

            int type = Util.inferContentType(uri.getLastPathSegment());
            switch (type) {
                case C.TYPE_SS:
                    return new SsMediaSource.Factory(
                            new DefaultSsChunkSource.Factory(mediaDataSourceFactory),
                            new DefaultDataSourceFactory(context, null, mediaDataSourceFactory))
                            .createMediaSource(uri);
                case C.TYPE_DASH:
                    return new DashMediaSource.Factory(
                            new DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                            new DefaultDataSourceFactory(context, null, mediaDataSourceFactory))
                            .createMediaSource(uri);
                case C.TYPE_HLS:
                    return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
                case C.TYPE_OTHER:
                    return new ExtractorMediaSource.Factory(mediaDataSourceFactory)
                            .setExtractorsFactory(new DefaultExtractorsFactory())
                            .createMediaSource(uri);
                default: {
                    throw new IllegalStateException("Unsupported type: " + type);
                }
            }
        }

        private void setupVideoPlayer(
                EventChannel eventChannel,
                TextureRegistry.SurfaceTextureEntry textureEntry,
                Result result) {

            eventChannel.setStreamHandler(
                    new EventChannel.StreamHandler() {
                        @Override
                        public void onListen(Object o, EventChannel.EventSink sink) {
                            eventSink.setDelegate(sink);
                        }

                        @Override
                        public void onCancel(Object o) {
                            eventSink.setDelegate(null);
                        }
                    });

            surface = new Surface(textureEntry.surfaceTexture());
            exoPlayer.setVideoSurface(surface);
            setAudioAttributes(exoPlayer);

            exoPlayer.addListener(
                    new EventListener() {

                        @Override
                        public void onPlayerStateChanged(final boolean playWhenReady, final int playbackState) {
                            if (playbackState == Player.STATE_BUFFERING) {
                                sendBufferingUpdate();
                            } else if (playbackState == Player.STATE_READY) {
                                if (!isInitialized) {
                                    isInitialized = true;
                                    sendInitialized();
                                }
                            } else if (playbackState == Player.STATE_ENDED) {
                                Map<String, Object> event = new HashMap<>();
                                event.put("event", "completed");
                                eventSink.success(event);
                            }
                        }

                        @Override
                        public void onPlayerError(final ExoPlaybackException error) {
                            if (eventSink != null) {
                                eventSink.error("VideoError", "Video player had error " + error, null);
                            }
                        }
                    });

            Map<String, Object> reply = new HashMap<>();
            reply.put("textureId", textureEntry.id());
            result.success(reply);
        }

        private void sendBufferingUpdate() {
            Map<String, Object> event = new HashMap<>();
            event.put("event", "bufferingUpdate");

            List<? extends Number> range = Arrays.asList(0, exoPlayer.getBufferedPosition());
            // iOS supports a list of buffered ranges, so here is a list with a single range.
            event.put("values", Collections.singletonList(range));
            eventSink.success(event);
        }

        private void sendBufferingUpdateCast() {
            Map<String, Object> event = new HashMap<>();
            event.put("event", "bufferingUpdate");

            List<? extends Number> range = Arrays.asList(0, playerManager.getBufferedPosition());
            // iOS supports a list of buffered ranges, so here is a list with a single range.
            event.put("values", Collections.singletonList(range));
            eventSink.success(event);
        }

        @SuppressWarnings("deprecation")
        private void setAudioAttributes(SimpleExoPlayer exoPlayer) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                exoPlayer.setAudioAttributes(
                        new AudioAttributes.Builder().setContentType(C.CONTENT_TYPE_MOVIE).build());
            } else {
                exoPlayer.setAudioStreamType(C.STREAM_TYPE_MUSIC);
            }
        }

        void play() {
            exoPlayer.setPlayWhenReady(true);
        }

        void pause() {
            exoPlayer.setPlayWhenReady(false);
        }

        void setLooping(boolean value) {
            exoPlayer.setRepeatMode(value ? REPEAT_MODE_ALL : REPEAT_MODE_OFF);
        }

        void setVolume(double value) {
            float bracketedValue = (float) Math.max(0.0, Math.min(1.0, value));
            exoPlayer.setVolume(bracketedValue);
        }

        void seekTo(int location) {
            exoPlayer.seekTo(location);
        }

        long getPosition() {
            return exoPlayer.getCurrentPosition();
        }

        @SuppressWarnings("SuspiciousNameCombination")
        private void sendInitialized() {
            if (isInitialized) {
                Map<String, Object> event = new HashMap<>();
                event.put("event", "initialized");
                event.put("duration", exoPlayer.getDuration());

                if (exoPlayer.getVideoFormat() != null) {
                    Format videoFormat = exoPlayer.getVideoFormat();
                    int width = videoFormat.width;
                    int height = videoFormat.height;
                    int rotationDegrees = videoFormat.rotationDegrees;
                    // Switch the width/height if video was taken in portrait mode
                    if (rotationDegrees == 90 || rotationDegrees == 270) {
                        width = exoPlayer.getVideoFormat().height;
                        height = exoPlayer.getVideoFormat().width;
                    }
                    event.put("width", width);
                    event.put("height", height);
                }
                eventSink.success(event);
            }
        }

        void dispose() {
            if (!connected) {
                if (isInitialized) {
                    exoPlayer.stop();
                }
                textureEntry.release();
                eventChannel.setStreamHandler(null);
                if (surface != null) {
                    surface.release();
                }
                if (exoPlayer != null) {
                    exoPlayer.release();
                }
            } else {

            }
        }
    }

    public static void registerWith(Registrar registrar) {
        final VideoPlayerPlugin plugin = new VideoPlayerPlugin(registrar);
        final MethodChannel channel =
                new MethodChannel(registrar.messenger(), "flutter.io/videoPlayer");
        channel.setMethodCallHandler(plugin);
        registrar.addViewDestroyListener(
                new PluginRegistry.ViewDestroyListener() {
                    @Override
                    public boolean onViewDestroy(FlutterNativeView view) {
                        plugin.onDestroy();
                        return false; // We are not interested in assuming ownership of the NativeView.
                    }
                });

        VideoPlayerDownloadService.setVideoPlayerPlugin(plugin);
        try {
            DownloadService.start(registrar.context(), VideoPlayerDownloadService.class);
        } catch (IllegalStateException e) {
            DownloadService.startForeground(registrar.context(), VideoPlayerDownloadService.class);
        }
    }

    public VideoPlayerPlugin(Registrar registrar) {
        mediaItemBuilder = new MediaItem.Builder();
        this.registrar = registrar;
        this.videoPlayers = new LongSparseArray<>();
        userAgent = Util.getUserAgent(registrar.context(), "VideoPlayer");
    }

    private final LongSparseArray<VideoPlayer> videoPlayers;

    private VideoDownloader videoDownloader;

    private final Registrar registrar;

    private void disposeAllPlayers() {
        for (int i = 0; i < videoPlayers.size(); i++) {
            videoPlayers.valueAt(i).dispose();
        }
        videoPlayers.clear();
    }

    private void disposeDownloader() {
        if (videoDownloader != null) {
            videoDownloader.dispose();
        }
    }

    private void onDestroy() {
//        disposeAllPlayers();
//        disposeDownloader();
//        playerManager.playpause(true);
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        TextureRegistry textures = registrar.textures();
        if (textures == null) {
            result.error("no_activity", "video_player plugin requires a foreground activity", null);
            return;
        }
        switch (call.method) {
            case "init":
                if (route == null) {
                    disposeAllPlayers();
                    disposeDownloader();
                } else {
//                    if (route.getConnectionState() == MediaRouter.RouteInfo.CONNECTION_STATE_CONNECTED) {
                        playerManager.release();
                        playerManager = null;
//                    } else {
                        disposeAllPlayers();
                        disposeDownloader();
//                    }
                }
                break;
            case "create": {
                TextureRegistry.SurfaceTextureEntry handle = textures.createSurfaceTexture();
                EventChannel eventChannel = new EventChannel(
                        registrar.messenger(),
                        "flutter.io/videoPlayer/videoEvents" + handle.id()
                );

                String argAsset = call.argument("asset");
                String argPackage = call.argument("package");

                String dataSource;
                String subtitleSource = call.argument("subtitle");
                String idDownload = call.argument("idDownload");
                VideoPlayer player;

                if (argAsset != null) {
                    String assetLookupKey;
                    if (argPackage != null) {
                        assetLookupKey = registrar.lookupKeyForAsset(argAsset, argPackage);
                    } else {
                        assetLookupKey = registrar.lookupKeyForAsset(argAsset);
                    }
                    dataSource = "asset:///" + assetLookupKey;
                } else {
                    dataSource = call.argument("uri");
                }

                player = new VideoPlayer(
                        registrar.context(),
                        eventChannel,
                        handle,
                        dataSource,
                        subtitleSource,
                        idDownload,
                        this,
                        result
                );
                videoPlayers.put(handle.id(), player);
                if(castContext != null) {
                String applicationId = castContext.getCastOptions().getReceiverApplicationId();
                switch (applicationId) {
                    case CastMediaControlIntent.DEFAULT_MEDIA_RECEIVER_APPLICATION_ID:
                        playerManager =
                                new DefaultReceiverPlayerManager(
                                        /* listener= */ this,
                                        /* context= */ registrar.context(),
                                        castContext, dataSource);
                        break;
                    default:
                        throw new IllegalStateException("Illegal receiver app id: " + applicationId);
                }
                }
                mCastSession = castContext.getSessionManager().getCurrentCastSession();
                if(mCastSession != null && mCastSession.isConnected()){
                    Map<String, Object> event = new HashMap<>();
                    event.put("event","getsession");
                    event.put("values",mCastSession.isConnected());
                    event.put("values_seek",playerManager.gettime());
                    eventSink.success(event);
                }

                mediaItemBuilder
                        .clear()
                        .setMedia(dataSource)
                        .setTitle("play")
                        .setMimeType("video/mp4");
                playerManager.addItem(mediaItemBuilder.build());

                break;
            }
            case "createDownload": {
                EventChannel eventChannel = new EventChannel(
                        registrar.messenger(),
                        "flutter.io/videoPlayer/downloadEvents"
                );

                videoDownloader = new VideoDownloader(eventChannel);
                result.success(null);
                break;
            }
            case "download": {
                Uri uri = Uri.parse(call.argument("url"));

                DownloadTracker dt = getDownloadTracker();
                RenderersFactory rf = buildRenderersFactory();
                final DownloadHelper dh = dt.getDownloadHelper(uri, rf);

                dh.prepare(new DownloadHelper.Callback() {
                    @Override
                    public void onPrepared(DownloadHelper helper) {
                        List<List<Integer>> tracks = call.argument("tracks");

                        if (tracks != null && tracks.size() > 0) {
                            for (int p = 0; p < dh.getPeriodCount(); p++) {
                                dh.clearTrackSelections(p);
                                for (int r = 0; r < dh.getMappedTrackInfo(p).getRendererCount(); r++) {

                                    List<DefaultTrackSelector.SelectionOverride> overrides = new ArrayList<>();
                                    for (int i = 0; i < tracks.size(); i++) {
                                        List<Integer> coord = tracks.get(i);
                                        int period = coord.get(0);
                                        int renderer = coord.get(1);
                                        int group = coord.get(2);
                                        int track = coord.get(3);

                                        if (period == p && renderer == r) {
                                            overrides.add(new DefaultTrackSelector.SelectionOverride(group, track));
                                            break;
                                        }
                                    }

                                    dh.addTrackSelectionForSingleRenderer(
                                            p,
                                            r,
                                            DownloadHelper.DEFAULT_TRACK_SELECTOR_PARAMETERS,
                                            overrides
                                    );
                                }
                            }
                        }

                        String id = call.argument("id");
                        if (id == null) id = uri.toString();

                        byte[] data = Util.getUtf8Bytes("Download Finish");
                        DownloadRequest dr = helper.getDownloadRequest(id, data);

                        DownloadService.sendAddDownload(
                                registrar.context(),
                                VideoPlayerDownloadService.class,
                                dr,
                                false);
                    }

                    @Override
                    public void onPrepareError(DownloadHelper helper, IOException e) {
                        result.error(TAG, "Error", e.toString());
                    }
                });
                break;
            }
            case "getStreamInfo": {
                Uri uri = Uri.parse(call.argument("url"));

                DownloadTracker dt = getDownloadTracker();
                RenderersFactory rf = buildRenderersFactory();
                DownloadHelper dh = dt.getDownloadHelper(uri, rf);
                dh.prepare(new DownloadHelper.Callback() {
                    @Override
                    public void onPrepared(DownloadHelper helper) {
                        List<Object> periods = new ArrayList<>();
                        for (int p = 0; p < helper.getPeriodCount(); p++) {
                            MappingTrackSelector.MappedTrackInfo mti = helper.getMappedTrackInfo(p);
                            List<Object> renderers = new ArrayList<>();
                            for (int r = 0; r < mti.getRendererCount(); r++) {
                                int rendererType = mti.getRendererType(r);
                                if (!(rendererType == C.TRACK_TYPE_VIDEO ||
                                        rendererType == C.TRACK_TYPE_AUDIO ||
                                        rendererType == C.TRACK_TYPE_TEXT))
                                    continue;

                                TrackGroupArray tga = mti.getTrackGroups(r);
                                List<Object> groups = new ArrayList<>();
                                for (int ga = 0; ga < tga.length; ga++) {
                                    TrackGroup tg = tga.get(ga);

                                    List<String> tracks = new ArrayList<>();
                                    for (int g = 0; g < tg.length; g++) {
                                        TrackNameProvider tnp = new DefaultTrackNameProvider(registrar.context().getResources());
                                        tracks.add(tnp.getTrackName(tg.getFormat(g)));
                                    }

                                    HashMap<String, Object> res = new HashMap<>();
                                    res.put("tracks", tracks);
                                    groups.add(res);
                                }

                                HashMap<String, Object> renderer = new HashMap<>();
                                renderer.put("groups", groups);
                                renderer.put("type", rendererType);
                                renderers.add(renderer);
                            }

                            HashMap<String, Object> res = new HashMap<>();
                            res.put("renderers", renderers);
                            periods.add(res);
                        }

                        HashMap<String, Object> res = new HashMap<>();
                        res.put("periods", periods);
                        result.success(res);
                    }

                    @Override
                    public void onPrepareError(DownloadHelper helper, IOException e) {

                    }
                });
                break;
            }
            case "getDownloadList": {
                ExoDatabaseProvider edp = new ExoDatabaseProvider(registrar.context());
                DefaultDownloadIndex di = new DefaultDownloadIndex(edp);

                try (DownloadCursor dc = di.getDownloads()) {
                    List<Object> info = new ArrayList<>();
                    while (dc.moveToNext()) {
                        Download download = dc.getDownload();

                        HashMap<String, String> item = new HashMap<>();
                        item.put("uri", download.request.uri.toString());
                        item.put("id", download.request.id);
                        item.put("state", String.valueOf(download.state));
                        item.put("progress", String.valueOf(download.getPercentDownloaded()));
                        item.put("byte", String.valueOf(download.getBytesDownloaded()));

                        info.add(item);
                    }
                    result.success(info);
                } catch (IOException e) {
                    Log.w(TAG, "Failed to query downloads", e);
                    result.error(TAG, "Error", e.toString());
                }

                break;
            }
            case "removeAllDownload": {
                DownloadService.sendRemoveAllDownloads(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        false);
                break;
            }
            case "removeDownload": {
                DownloadService.sendRemoveDownload(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        call.argument("id"),
                        false);
                break;
            }
            case "pauseAllDownload": {
                DownloadService.sendPauseDownloads(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        false);
                break;
            }
            case "resumeAllDownload": {
                DownloadService.sendResumeDownloads(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        false);
                break;
            }
            case "pauseDownload": {
                DownloadService.sendSetStopReason(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        call.argument("id"),
                        1000,
                        false);
                break;
            }
            case "resumeDownload": {
                DownloadService.sendSetStopReason(
                        registrar.context(),
                        VideoPlayerDownloadService.class,
                        call.argument("id"),
                        Download.STOP_REASON_NONE,
                        false);
                break;
            }
            case "setMaxParallelDownloads": {
                getDownloadManager().setMaxParallelDownloads(call.argument("maxParallelDownloads"));
                break;
            }
            case "disposeDownloader": {
                videoDownloader.dispose();
                result.success(null);
                break;
            }
            default: {
                long textureId = ((Number) call.argument("textureId")).longValue();
                VideoPlayer player = videoPlayers.get(textureId);
                if (player == null) {
                    result.error(
                            "Unknown textureId",
                            "No video player associated with texture id " + textureId,
                            null);
                    return;
                }
                onMethodCall(call, result, textureId, player);
                break;
            }
        }
    }

    public DataSource.Factory buildCacheHttpDataSourceFactory() {
        DefaultDataSourceFactory upstreamFactory = new DefaultDataSourceFactory(
                registrar.context(),
                buildHttpDataSourceFactory()
        );

        return buildReadOnlyCacheDataSource(upstreamFactory, getDownloadCache());
    }

    private HttpDataSource.Factory buildHttpDataSourceFactory() {
        return new DefaultHttpDataSourceFactory(
                userAgent,
                null,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true
        );
    }

    public RenderersFactory buildRenderersFactory() {
        return new DefaultRenderersFactory(registrar.context())
                .setExtensionRendererMode(EXTENSION_RENDERER_MODE_OFF);
    }

    public DownloadManager getDownloadManager() {
        initDownloadManager();
        return downloadManager;
    }

    public DownloadTracker getDownloadTracker() {
        initDownloadManager();
        return downloadTracker;
    }

    protected synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File file;
            if(castContext.getCastState() != 4){
                file = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            }else {
                file = new File(getDownloadDirectory(), "downloads");
            }
            downloadCache = new SimpleCache(file, new NoOpCacheEvictor(), getDatabaseProvider());
        }

        return downloadCache;
    }

    private synchronized void initDownloadManager() {
        if (downloadManager == null) {
            DefaultDownloadIndex di = new DefaultDownloadIndex(getDatabaseProvider());
            DownloaderConstructorHelper dch = new DownloaderConstructorHelper(
                    getDownloadCache(),
                    buildHttpDataSourceFactory()
            );
            downloadManager = new DownloadManager(
                    registrar.context(),
                    di,
                    new DefaultDownloaderFactory(dch)
            );
            downloadTracker = new DownloadTracker(buildCacheHttpDataSourceFactory(), downloadManager);
        }
    }

    private DatabaseProvider getDatabaseProvider() {
        if (databaseProvider == null) {
            databaseProvider = new ExoDatabaseProvider(registrar.context());
        }

        return databaseProvider;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = registrar.context().getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = registrar.context().getFilesDir();
            }
        }
        return downloadDirectory;
    }

    protected static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DataSource.Factory upstreamFactory, Cache cache) {
        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                new FileDataSourceFactory(),
                null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                null);
    }

    private void onMethodCall(MethodCall call, Result result, long textureId, VideoPlayer player) {
        switch (call.method) {
            case "setLooping":
                player.setLooping(call.argument("looping"));
                result.success(null);
                break;
            case "setVolume":
                player.setVolume(call.argument("volume"));
                result.success(null);
                break;
            case "setCastVolume":
                playerManager.setVolume(call.argument("volume"));
                result.success(null);
                break;
            case "setCastMute":
                playerManager.setMute(call.argument("ismute"));
                result.success(null);
                break;
            case "getCastVolume":
                double vol = playerManager.getVolume();
                result.success(vol);
                break;
            case "play":
                player.play();
                result.success(null);
                break;
            case "pause":
                player.pause();
                result.success(null);
                break;
            case "seekTo":
                int location = ((Number) call.argument("location")).intValue();
                player.seekTo(location);
                result.success(null);
                break;
            case "position":
                if (call.argument("isconnect")) {
                    result.success(Long.parseLong(String.valueOf(playerManager.gettime())));
                    player.sendBufferingUpdateCast();
                } else {
                    result.success(player.getPosition());
                    player.sendBufferingUpdate();
                }
                break;
            case "dispose":
                player.dispose();
                videoPlayers.remove(textureId);
                result.success(null);
                break;
            case "cast_connect":
                player.pause();
                route = mRouter.updateSelectedRoute(mSelector);
                mRouter.selectRoute(routeInfos.get(call.argument("index")));
                playerManager.selectQueueItem(0, Long.parseLong(call.argument("location").toString()));
                connected = true;
                Map<String, Object> event = new HashMap<>();
                event.put("event", "play");
                event.put("status", true);
                eventSink.success(event);
                break;
            case "cast_play":
                boolean stat = call.argument("play");
                playerManager.playpause(stat);
                break;
            case "cast_disconnect":
                int position = playerManager.gettime();
                connected = false;
                player.seekTo(position);
                player.play();
                mRouter.unselect(MediaRouter.UNSELECT_REASON_DISCONNECTED);
                mRouter.updateSelectedRoute(mSelector);
                break;
            case "cast_seek_to":
                playerManager.seekto(0, Long.parseLong(call.argument("location").toString()));
                break;
            case "setSub":
                playerManager.setSub(1);
                result.success(null);
                break;
            case "widgetpause":
                playerManager.release();
                break;
            case "widgetresume":
                playerManager.onresume();
                break;
            case "widgetstop":
                return;
            default:
                result.notImplemented();
                break;
        }
    }

}
