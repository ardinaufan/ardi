/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.flutter.plugins.videoplayer;

import android.content.Context;
import android.view.KeyEvent;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.DiscontinuityReason;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.Player.TimelineChangeReason;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Timeline.Period;
import extcast.CastPlayer;
import extcast.MediaItem;
import extcast.SessionAvailabilityListener;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.util.Log;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Manages players and an internal media queue for the ExoPlayer/Cast demo app. */
class DefaultReceiverPlayerManager implements PlayerManager, EventListener, SessionAvailabilityListener {

  private static String link;
  private final CastPlayer castPlayer;
  private final ArrayList<MediaItem> mediaQueue;
  private final Listener listener;
  private final ConcatenatingMediaSource concatenatingMediaSource;



  private int currentItemIndex;
  private Player currentPlayer;
  private long location;
  private Context context;

  /**
   * Creates a new manager for {@link SimpleExoPlayer} and {@link CastPlayer}.
   * @param listener A {@link Listener} for queue position changes.
   * @param context A {@link Context}.
   * @param castContext The {@link CastContext}.
   * @param uri
   */
  public DefaultReceiverPlayerManager(
          VideoPlayerPlugin listener,
          Context context,
          CastContext castContext, String uri) {

    link = uri;
    this.listener = listener;
    mediaQueue = new ArrayList<>();
    currentItemIndex = C.INDEX_UNSET;
    concatenatingMediaSource = new ConcatenatingMediaSource();

    castPlayer = new CastPlayer(castContext);
    castPlayer.addListener(this);
    castPlayer.setSessionAvailabilityListener(this);

    this.context = context;
//    castControlView.setPlayer(castPlayer);
//    currentPlayer = castPlayer;
    setCurrentPlayer(castPlayer);
  }

  // Queue manipulation methods.

  /**
   * Plays a specified queue item in the current player.
   *
   * @param itemIndex The index of the item to play.
   */
  @Override
  public void selectQueueItem(int itemIndex,long position) {
    location = position;
    setCurrentItem(itemIndex, position, true);
  }

  @Override
  public void seekto(int position, long positions) {
    seekto(position,positions,true);
  }

  /** Returns the index of the currently played item. */
  @Override
  public int getCurrentItemIndex() {
    return currentItemIndex;
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent keyEvent) {
    return false;
  }

  /**
   * Appends {@code item} to the media queue.
   *
   * @param item The {@link MediaItem} to append.
   */
  @Override
  public void addItem(MediaItem item) {
    mediaQueue.add(item);
      try {
        castPlayer.addItems(buildMediaQueueItem(item));
      }catch (Exception e){
        e.printStackTrace();
      }
  }

  /** Returns the size of the media queue. */
  @Override
  public int getMediaQueueSize() {
    return mediaQueue.size();
  }

  /**
   * Returns the item at the given index in the media queue.
   *
   * @param position The index of the item.
   * @return The item at the given index in the media queue.
   */
  @Override
  public MediaItem getItem(int position) {
    return mediaQueue.get(position);
  }


  /**
   * Removes the item at the given index from the media queue.
   *
   * @param item The item to remove.
   * @return Whether the removal was successful.
   */
  @Override
  public boolean removeItem(MediaItem item) {
    int itemIndex = mediaQueue.indexOf(item);
    if (itemIndex == -1) {
      return false;
    }
    concatenatingMediaSource.removeMediaSource(itemIndex);
    if (currentPlayer == castPlayer) {
      if (castPlayer.getPlaybackState() != Player.STATE_IDLE) {
        Timeline castTimeline = castPlayer.getCurrentTimeline();
        if (castTimeline.getPeriodCount() <= itemIndex) {
          return false;
        }
        castPlayer.removeItem((int) castTimeline.getPeriod(itemIndex, new Period()).id);
      }
    }
    mediaQueue.remove(itemIndex);
    if (itemIndex == currentItemIndex && itemIndex == mediaQueue.size()) {
      maybeSetCurrentItemAndNotify(C.INDEX_UNSET);
    } else if (itemIndex < currentItemIndex) {
      maybeSetCurrentItemAndNotify(currentItemIndex - 1);
    }
    return true;
  }

  /**
   * Moves an item within the queue.
   *
   * @param item The item to move.
   * @param toIndex The target index of the item in the queue.
   * @return Whether the item move was successful.
   */
  @Override
  public boolean moveItem(MediaItem item, int toIndex) {
    int fromIndex = mediaQueue.indexOf(item);
    if (fromIndex == -1) {
      return false;
    }
    // Player update.
    concatenatingMediaSource.moveMediaSource(fromIndex, toIndex);
    if (currentPlayer == castPlayer && castPlayer.getPlaybackState() != Player.STATE_IDLE) {
      Timeline castTimeline = castPlayer.getCurrentTimeline();
      int periodCount = castTimeline.getPeriodCount();
      if (periodCount <= fromIndex || periodCount <= toIndex) {
        return false;
      }
      int elementId = (int) castTimeline.getPeriod(fromIndex, new Period()).id;
      castPlayer.moveItem(elementId, toIndex);
    }

    mediaQueue.add(toIndex, mediaQueue.remove(fromIndex));

    // Index update.
    if (fromIndex == currentItemIndex) {
      maybeSetCurrentItemAndNotify(toIndex);
    } else if (fromIndex < currentItemIndex && toIndex >= currentItemIndex) {
      maybeSetCurrentItemAndNotify(currentItemIndex - 1);
    } else if (fromIndex > currentItemIndex && toIndex <= currentItemIndex) {
      maybeSetCurrentItemAndNotify(currentItemIndex + 1);
    }
    return true;
  }

  /**
   * Dispatches a given {@link KeyEvent} to the corresponding view of the current player.
   *
   * @param event The {@link KeyEvent}.
   * @return Whether the event was handled by the target view.
   */

  /** Releases the manager and the players that it holds. */
  @Override
  public void release() {
    Log.e("DRPM", "release: ");
    currentItemIndex = C.INDEX_UNSET;
    mediaQueue.clear();
    concatenatingMediaSource.clear();
    castPlayer.setSessionAvailabilityListener(null);
    castPlayer.release();
  }

  @Override
  public void onresume() {
    castPlayer.onResume();
  }

  // Player.EventListener implementation.

  @Override
  public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
    updateCurrentItemIndex();
  }

  @Override
  public void onPositionDiscontinuity(@DiscontinuityReason int reason) {
    updateCurrentItemIndex();
  }

  @Override
  public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, @TimelineChangeReason int reason) {
    updateCurrentItemIndex();
  }

  // CastPlayer.SessionAvailabilityListener implementation.

  @Override
  public void onCastSessionAvailable() {
    selectQueueItem(0,location);
  }

  @Override
  public void onCastSessionUnavailable() {

  }
  // Internal methods.

  private void updateCurrentItemIndex() {
    int playbackState = currentPlayer.getPlaybackState();
    maybeSetCurrentItemAndNotify(
        playbackState != Player.STATE_IDLE && playbackState != Player.STATE_ENDED
            ? currentPlayer.getCurrentWindowIndex()
            : C.INDEX_UNSET);
  }

  //setcurrentplayer
  private void setCurrentPlayer(Player currentPlayer) {
    if (this.currentPlayer == currentPlayer) {
      return;
    }
    // Player state management.
    long playbackPositionMs = C.TIME_UNSET;
    int windowIndex = C.INDEX_UNSET;
    boolean playWhenReady = true;
    if (this.currentPlayer != null) {
      int playbackState = this.currentPlayer.getPlaybackState();
      if (playbackState != Player.STATE_ENDED) {
        playbackPositionMs = this.currentPlayer.getCurrentPosition();
        playWhenReady = true;
        windowIndex = this.currentPlayer.getCurrentWindowIndex();
        if (windowIndex != currentItemIndex) {
          playbackPositionMs = C.TIME_UNSET;
          windowIndex = currentItemIndex;
        }
      }
      this.currentPlayer.stop(true);
    } else {
      // This is the initial setup. No need to save any state.
    }

    this.currentPlayer = currentPlayer;

    // Playback transition.
    if (windowIndex != C.INDEX_UNSET) {
      setCurrentItem(windowIndex, location, playWhenReady);
    }
  }

  @Override
  public int gettime() {
    int dur;
    dur = (int) castPlayer.getCurrentPosition();
    return dur;
  }

  @Override
  public Long getBufferedPosition() {
    return castPlayer.getBufferedPosition();
  }

  /**
   * Starts playback of the item at the given position.
   *
   * @param itemIndex The index of the item to play.
   * @param positionMs The position at which playback should start.
   * @param playWhenReady Whether the player should proceed when ready to do so.
   */

  private void seekto(int itemIndex, long positionMs, boolean playWhenReady) {
    castPlayer.seekTo(itemIndex,positionMs);
    castPlayer.setPlayWhenReady(playWhenReady);
  }


  private void setCurrentItem(int itemIndex, long positionMs, boolean playWhenReady) {
    maybeSetCurrentItemAndNotify(itemIndex);
    if (currentPlayer == castPlayer && castPlayer.getCurrentTimeline().isEmpty()){
      MediaQueueItem[] items = new MediaQueueItem[mediaQueue.size()];
      for (int i = 0; i < items.length; i++) {
        items[i] = buildMediaQueueItem(mediaQueue.get(i));
      }
      castPlayer.loadItems(items, itemIndex, positionMs, Player.REPEAT_MODE_OFF);
      castPlayer.setPlayWhenReady(playWhenReady);
    } else {
      currentPlayer.seekTo(itemIndex, positionMs);
      currentPlayer.setPlayWhenReady(playWhenReady);
    }
  }

  private void maybeSetCurrentItemAndNotify(int currentItemIndex) {
    if (this.currentItemIndex != currentItemIndex) {
      int oldIndex = this.currentItemIndex;
      this.currentItemIndex = currentItemIndex;
      listener.onQueuePositionChanged(oldIndex, currentItemIndex);
    }
  }

  private static MediaQueueItem buildMediaQueueItem(MediaItem item) {
    try {
      List tracks = new ArrayList();
    MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
    movieMetadata.putString(MediaMetadata.KEY_TITLE, item.title);
    movieMetadata.putString(MediaMetadata.KEY_SUBTITLE,"studio");
    MediaTrack englishSubtitle = new MediaTrack.Builder(1 /* ID */,
              MediaTrack.TYPE_TEXT)
              .setName("English Subtitle")
              .setSubtype(MediaTrack.SUBTYPE_SUBTITLES)
              .setContentId("http://dev.motion.co.id/Video/sub.vtt")
              /* language is required for subtitle type but optional otherwise */
              .setLanguage("id")
              .build();
    tracks.add(englishSubtitle);
    MediaInfo mediaInfo = new MediaInfo.Builder(item.media.uri.toString())
        .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
            .setContentType("video/mp4")
            .setMetadata(movieMetadata)
            .setMediaTracks(tracks)
            .build();
    return new MediaQueueItem.Builder(mediaInfo).build();
    }catch (Exception e){
     return null;
    }
  }

  @Override
  public void playpause(boolean status){
    castPlayer.setPlayWhenReady(status);
  }

  @Override
  public void setMute(boolean volume) {
    castPlayer.setMute(volume);
  }

  @Override
  public double getVolume() {
    double volume = castPlayer.getVolume();
    Log.e("GET VOLUME NATIVE", String.valueOf(volume));
    return volume;
  }

  @Override
  public void setVolume(double volume) {
    castPlayer.setVolume(volume);
  }

  @Override
  public void setSub(long id) {
    castPlayer.setSub(id,this.context);
  }
}
