package io.flutter.plugins.videoplayer;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.OptionsProvider;
import com.google.android.gms.cast.framework.SessionProvider;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.NotificationOptions;

import java.util.List;

import io.flutter.plugin.common.PluginRegistry;

/**
 * A convenience {@link OptionsProvider} to target the default cast receiver app.
 */
public final class DefaultCastOptionProvider implements OptionsProvider {

    @Override
    public CastOptions getCastOptions(Context context) {

        Log.e( "getCastOptions: ",VideoPlayerPlugin.VideoPlayer.class.getName());
        NotificationOptions notificationOptions = new NotificationOptions.Builder()
                .setTargetActivityClassName(VideoPlayerPlugin.VideoPlayer.class.getName())
                .build();
        CastMediaOptions mediaOptions = new CastMediaOptions.Builder()
                .setNotificationOptions(notificationOptions)
                .setExpandedControllerActivityClassName(VideoPlayerPlugin.VideoPlayer.class.getName())
                .build() ;

        return new CastOptions.Builder()
                .setReceiverApplicationId(CastMediaControlIntent.DEFAULT_MEDIA_RECEIVER_APPLICATION_ID)
                .setCastMediaOptions(mediaOptions)
                .build();
    }

    @Override
    public List<SessionProvider> getAdditionalSessionProviders(Context context) {
        return null;
    }

}

